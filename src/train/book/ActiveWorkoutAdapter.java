package train.book;

import java.util.Date;
import java.util.List;

import train.book.data.Workout;
import android.content.Context;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ActiveWorkoutAdapter extends ArrayAdapter<Pair<Workout, Long>> {

	public ActiveWorkoutAdapter(Context context, List<Pair<Workout, Long>> list) {
		super(context, R.layout.active_workout_table_row, list);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = View.inflate(getContext(),
					R.layout.active_workout_table_row, null);
		}
		Pair<Workout, Long> p = getItem(position);
		if (p != null) {
			Workout w = p.first;
			if (w != null) {
				TextView name = (TextView) convertView
						.findViewById(R.id.textViewActiveWorkoutTableName);
				TextView date = (TextView) convertView
						.findViewById(R.id.textViewActiveWorkoutTableDate);
				name.setText(w.getName());

				long lastTime = p.second;
				if (lastTime == 0) {
					date.setVisibility(View.GONE);
				} else {
					String dateText = getContext().getString(
							R.string.workout_latest)
							+ ": "
							+ Const.date_medium_format
									.format(new Date(lastTime));
					date.setVisibility(View.VISIBLE);
					date.setText(dateText);
				}
			}
		}
		return convertView;
	}
}
