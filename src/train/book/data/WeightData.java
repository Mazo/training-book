package train.book.data;

import java.io.IOException;
import java.util.Date;
import java.util.Scanner;

import train.book.Const;

public class WeightData implements HistoryData {
	private double weight = Const.DEFAULT_WEIGHT;
	private Date startDate;
	private long id = -1;

	public WeightData(Date startDate, double weight) {
		this.startDate = startDate;
		this.weight = weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getWeight() {
		return weight;
	}

	public long getTime() {
		return startDate.getTime();
	}

	public void setTime(long time) {
		startDate = new Date(time);
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public static WeightData parse(String s) throws IOException {
		Scanner sc = new Scanner(s);
		if (sc.hasNextLong()) {
			Date time = new Date(sc.nextLong());
			if (sc.hasNextDouble()) {
				double w = sc.nextDouble();
				return new WeightData(time, w);
			}
		}
		throw new IOException("Unable to parse: " + s);
	}

	public String emit() {
		return startDate.getTime() + " " + weight;
	}
}
