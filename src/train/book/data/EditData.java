package train.book.data;

public interface EditData {
	public void commit();

	public void revert();

	public void startEditing();

	public enum EditType {
		NEW, CHANGED, UNCHANGED, DELETED, UNKNOWN;
	}
}
