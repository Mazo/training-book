package train.book.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import train.book.Const;

public class WorkoutData implements Comparable<WorkoutData>, EditData,
		HistoryData {
	private ArrayList<ExerciseData> r;
	private Date startDate;
	private long id = -1;
	private long workoutId;
	private String name = Const.UNKNOWN_NAME;
	private String comment = "";
	private WorkoutData backup = null;

	public WorkoutData(long workoutId, Date startDate) {
		r = new ArrayList<ExerciseData>();
		this.startDate = startDate;
		this.workoutId = workoutId;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getComment() {
		return comment;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWorkoutName() {
		return name;
	}

	public void addData(ExerciseData data) {
		r.add(data);
	}

	public ExerciseData get(int i) {
		return r.get(i);
	}

	public void delete(ExerciseData d) {
		long dId = d.getId();
		for (int i = 0; i < r.size(); i++) {
			if (r.get(i).getId() == dId) {
				r.remove(i);
				break;
			}
		}
	}

	public long getWorkoutId() {
		return workoutId;
	}

	public int size() {
		return r.size();
	}

	@Override
	public String toString() {
		String s = startDate.toLocaleString();
		for (ExerciseData e : r) {
			s += "\n" + e.toString();
		}
		return s;
	}

	public long getTime() {
		return startDate.getTime();
	}

	public void setTime(long time) {
		startDate = new Date(time);
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	private WorkoutData getCopy() {
		WorkoutData copy = new WorkoutData(workoutId, startDate);
		copy.setId(id);
		copy.setName(name);
		copy.setComment(comment);

		for (ExerciseData e : r) {
			copy.r.add(e.getCopy());
		}

		return copy;
	}

	private boolean dataChanged(WorkoutData another) {
		if (r.size() != another.r.size()) {
			return true;
		} else {
			// Equal size
			for (int i = 0; i < r.size(); i++) {
				if (r.get(i).compareTo(another.r.get(i)) != 0) {
					return true;
				}
			}
		}
		return false;
	}

	public int compareTo(WorkoutData another) {
		int cmp1 = name.compareTo(another.name);
		if (cmp1 != 0) {
			return cmp1;
		}
		int cmp2 = new Long(id).compareTo(another.id);
		if (cmp2 != 0) {
			return cmp2;
		}
		int cmp3 = new Long(workoutId).compareTo(another.workoutId);
		if (cmp3 != 0) {
			return cmp3;
		}
		int cmp4 = comment.compareTo(another.comment);
		if (cmp4 != 0) {
			return cmp4;
		}
		int cmp5 = startDate.compareTo(another.startDate);
		if (cmp5 != 0) {
			return cmp5;
		}
		return dataChanged(another) ? -1 : 0;
	}

	public void commit() {
		if (backup != null) {
			if (id == -1) {
				// New
			} else if (compareTo(backup) != 0) {
				// Changed
			}
			backup = null;
		}
	}

	public void revert() {
		if (backup != null) {
			name = backup.name;
			id = backup.id;
			workoutId = backup.workoutId;
			comment = backup.comment;
			startDate = backup.startDate;

			r = backup.r;
			// r.clear();
			// for (ExerciseData d : backup.r) {
			// addData(d);
			// }
			backup = null;
		}
	}

	public void startEditing() {
		if (backup == null) {
			backup = getCopy();
		}
	}

	public static WorkoutData parse(String s, ArrayList<Workout> workouts,
			ArrayList<Exercise> exercises) throws IOException {
		Scanner sc = new Scanner(s);
		if (sc.hasNextInt()) {
			int index = sc.nextInt();
			if (sc.hasNextLong()) {
				Date time = new Date(sc.nextLong());
				WorkoutData res = new WorkoutData(workouts.get(index).getId(),
						time);

				sc.useDelimiter(";");
				if (sc.hasNext()) {
					res.setComment(sc.next().trim());
				} else {
					throw new IOException("Unable to parse: " + s);
				}

				while (sc.hasNext()) {
					ExerciseData d = ExerciseData.parse(sc.next(), exercises);
					res.addData(d);
				}

				return res;
			}
		}

		throw new IOException("Unable to parse: " + s);
	}

	public String emit(ArrayList<Workout> workouts,
			ArrayList<Exercise> exercises) throws IOException {
		int index = -1;
		for (int i = 0; i < workouts.size(); i++) {
			if (workouts.get(i).getId() == workoutId) {
				index = i;
				break;
			}
		}
		if (index == -1) {
			throw new IOException("Unable to emit: " + toString());
		}
		String out = index + " " + startDate.getTime() + " "
				+ comment.replace(';', ',').replace('\n', ' ') + ";";
		for (ExerciseData d : r) {
			out += d.emit(exercises) + ";";
		}
		return out;
	}
}
