package train.book.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import train.book.Const;

public class ExerciseData implements Comparable<ExerciseData>, EditData {
	private ArrayList<ExerciseSet> r = new ArrayList<ExerciseSet>();
	private long exerciseId = -1;
	private long id = -1;
	private String name;
	private ExerciseType t;
	private boolean isEditing = false;
	private ExerciseData backup;

	public ExerciseData(long exerciseId, ExerciseType t, String name) {
		this.exerciseId = exerciseId;
		this.t = t;
		this.name = name;
	}

	public ExerciseData(long exerciseId) {
		this(exerciseId, ExerciseType.UNKNOWN, Const.UNKNOWN_NAME);
	}

	public ExerciseType getType() {
		return t;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExerciseName() {
		return name;
	}

	public void addSet(ExerciseSet s) {
		r.add(s);
	}

	public void remove(int index) {
		r.remove(index);
	}

	public long getExerciseId() {
		return exerciseId;
	}

	public int size() {
		return r.size();
	}

	public ExerciseSet get(int i) {
		return r.get(i);
	}

	@Override
	public String toString() {
		String s = ""; // "Sets: " + r.size();
		boolean first = true;
		for (ExerciseSet e : r) {
			s += (first ? "" : "\n") + e.toString();
			first = false;
		}
		return s;
	}

	public long getD1Max() {
		long max = 0;
		for (ExerciseSet s : r) {
			max = Math.max(max, s.getD1());
		}
		return max;
	}

	public double getD2Max() {
		double max = 0;
		for (ExerciseSet s : r) {
			max = Math.max(max, s.getD2());
		}
		return max;
	}

	public void setType(ExerciseType t) {
		this.t = t;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public ExerciseData getCopy() {
		ExerciseData copy = new ExerciseData(exerciseId, t, name);
		copy.setId(id);

		for (ExerciseSet s : r) {
			copy.addSet(s.getCopy());
		}
		return copy;
	}

	private boolean setChanged(ExerciseData another) {
		if (r.size() != another.r.size()) {
			return true;
		} else {
			// Equal size
			for (int i = 0; i < r.size(); i++) {
				if (r.get(i).compareTo(another.r.get(i)) != 0) {
					return true;
				}
			}
		}
		return false;
	}

	public int compareTo(ExerciseData another) {
		int cmp1 = new Long(id).compareTo(another.id);
		if (cmp1 != 0) {
			return cmp1;
		}
		int cmp2 = name.compareTo(another.name);
		if (cmp2 != 0) {
			return cmp2;
		}
		int cmp3 = new Long(exerciseId).compareTo(another.exerciseId);
		if (cmp3 != 0) {
			return cmp3;
		}
		int cmp4 = t.compareTo(another.t);
		if (cmp4 != 0) {
			return cmp4;
		}
		return setChanged(another) ? -1 : 0;
	}

	public void commit() {
		if (isEditing) {
			isEditing = false;
			if (id == -1) {
				// New
			} else if (compareTo(backup) != 0) {
				// Changed
			}
			backup = null;
		}
	}

	public void revert() {
		if (isEditing) {
			isEditing = false;
			name = backup.name;
			id = backup.id;
			exerciseId = backup.exerciseId;
			t = backup.t;

			r = backup.r;
			// r.clear();
			// for (ExerciseSet s : backup.r) {
			// addSet(s);
			// }
			backup = null;
		}
	}

	public void startEditing() {
		if (!isEditing) {
			isEditing = true;
			backup = getCopy();
		}
	}

	public static ExerciseData parse(String s, ArrayList<Exercise> exercises)
			throws IOException {
		Scanner sc = new Scanner(s);
		if (sc.hasNextInt()) {
			int index = sc.nextInt();
			if (sc.hasNextInt()) {
				ExerciseType t = ExerciseType.getType(sc.nextInt());
				ExerciseData d = new ExerciseData(exercises.get(index).getId(),
						t, Const.UNKNOWN_NAME);
				while (sc.hasNextLong()) {
					long d1 = sc.nextLong();
					if (sc.hasNextDouble()) {
						double d2 = sc.nextDouble();
						d.addSet(new ExerciseSet(d1, d2, t));
					} else {
						throw new IOException("Unable to parse: " + s);
					}
				}
				return d;
			}
		}
		throw new IOException("Unable to parse: " + s);
	}

	public String emit(ArrayList<Exercise> exercises) throws IOException {
		int index = -1;
		for (int i = 0; i < exercises.size(); i++) {
			if (exercises.get(i).getId() == exerciseId) {
				index = i;
				break;
			}
		}
		if (index == -1) {
			throw new IOException("Unable to emit: " + toString());
		}
		String out = index + " " + t.getValue() + " ";
		for (ExerciseSet s : r) {
			out += s.getD1() + " " + s.getD2() + " ";
		}
		return out;
	}
}
