package train.book.data;

public class ActiveExerciseData extends ExerciseData implements D1D2Data {
	private WorkoutData w;
	private long d1Value = 0;
	private double d2Value = 0;
	private boolean isFirst = true;
	private ExerciseData previous = null;

	public ActiveExerciseData(WorkoutData w, long exerciseId, ExerciseType t,
			String name) {
		super(exerciseId, t, name);
		this.w = w;
	}

	public String getTitle(int index) {
		return (index + 1) + "/" + w.size() + " "
				+ w.get(index).getExerciseName();
	}

	public long getTime() {
		return w.getTime();
	}

	public long getWorkoutId() {
		return w.getWorkoutId();
	}

	public void setD1Value(long d1) {
		d1Value = d1;
	}

	public void setD2Value(double d2) {
		d2Value = d2;
	}

	public long getD1Value() {
		return d1Value;
	}

	public double getD2Value() {
		return d2Value;
	}

	public boolean isFirst() {
		return isFirst;
	}

	public void setFirst(boolean isFirst) {
		this.isFirst = isFirst;
	}

	public void setPrevious(ExerciseData previous) {
		this.previous = previous;
	}

	public ExerciseData getPrevious() {
		return previous;
	}
}
