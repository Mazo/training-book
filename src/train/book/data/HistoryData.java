package train.book.data;

public interface HistoryData {
	public long getTime();

	public void setTime(long time);
}
