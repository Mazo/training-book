package train.book.data;

public class EditExerciseData implements D1D2Data, EditData {
	private long d1Value;
	private double d2Value;
	private ExerciseData data;

	public EditExerciseData(ExerciseData data) {
		this.data = data;
		d1Value = data.getD1Max();
		d2Value = data.getD2Max();
	}

	public ExerciseSet get(int i) {
		return data.get(i);
	}

	public void remove(int index) {
		data.remove(index);
	}

	public void addSet(ExerciseSet s) {
		data.addSet(s);
	}

	public String getExerciseName() {
		return data.getExerciseName();
	}

	public int size() {
		return data.size();
	}

	public long getId() {
		return data.getId();
	}

	public ExerciseType getType() {
		return data.getType();
	}

	public long getExerciseId() {
		return data.getExerciseId();
	}

	public void setD1Value(long d1) {
		d1Value = d1;
	}

	public void setD2Value(double d2) {
		d2Value = d2;
	}

	public long getD1Value() {
		return d1Value;
	}

	public double getD2Value() {
		return d2Value;
	}

	public void commit() {
		data.commit();
	}

	public void revert() {
		data.revert();
	}

	public void startEditing() {
		data.startEditing();
	}
}
