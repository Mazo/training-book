package train.book.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Workout implements Comparable<Workout>, EditData {
	private String name;
	private ArrayList<Exercise> exercises = new ArrayList<Exercise>();
	private long id = -1;
	private long prio = 0;
	private boolean show = true;
	private Workout backup = null;

	public Workout(String name) {
		this.name = name;
	}

	public int getNumberOfExercises() {
		return exercises.size();
	}

	public void addExercise(Exercise e) {
		exercises.add(e);
	}

	@Override
	public String toString() {
		return name;
	}

	public Exercise getExercise(int index) {
		return exercises.get(index);
	}

	public void moveExercise(Exercise d, int i) {
		int index = exercises.indexOf(d);
		if (index != -1) {
			exercises.remove(index);
			int newIndex = index + i;
			if (newIndex < 0 || newIndex > exercises.size()) {
				exercises.add(index, d);
			} else {
				exercises.add(newIndex, d);
			}
		}
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public long getPrio() {
		return prio;
	}

	public boolean doShow() {
		return show;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setPrio(long prio) {
		this.prio = prio;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setShow(boolean show) {
		this.show = show;
	}

	private Workout getCopy() {
		Workout c = new Workout(name);
		c.id = id;
		c.prio = prio;
		c.show = show;
		for (Exercise e : exercises) {
			c.addExercise(e);
		}
		return c;
	}

	public void clearExercises() {
		exercises.clear();
	}

	private boolean exercisesChanged(Workout another) {
		if (exercises.size() != another.exercises.size()) {
			return true;
		} else {
			// Equal size
			for (int i = 0; i < exercises.size(); i++) {
				if (exercises.get(i).compareTo(another.exercises.get(i)) != 0) {
					return true;
				}
			}
		}
		return false;
	}

	public int compareTo(Workout another) {
		int cmp1 = name.compareTo(another.name);
		if (cmp1 != 0) {
			return cmp1;
		}
		int cmp2 = Long.valueOf(id).compareTo(another.id);
		if (cmp2 != 0) {
			return cmp2;
		}
		int cmp3 = Long.valueOf(prio).compareTo(another.prio);
		if (cmp3 != 0) {
			return cmp3;
		}
		return exercisesChanged(another) ? -1 : 0;
	}

	public void commit() {
		if (backup != null) {
			if (id == -1) {
				// New
			} else if (compareTo(backup) != 0) {
				// Changed
			}
			backup = null;
		}
	}

	public void revert() {
		if (backup != null) {
			name = backup.name;
			prio = backup.prio;
			show = backup.show;
			id = backup.id;
			exercises = backup.exercises;
			// exercises.clear();
			// for (Exercise e : backup.exercises) {
			// addExercise(e);
			// }
			backup = null;
		}
	}

	public void startEditing() {
		if (backup == null) {
			backup = getCopy();
		}
	}

	public static Workout parse(String s, ArrayList<Exercise> allExercises)
			throws IOException {
		Scanner sc = new Scanner(s);
		Workout w = new Workout("");

		while (sc.hasNextInt()) {
			w.addExercise(allExercises.get(sc.nextInt()));
		}
		if (sc.hasNextLine()) {
			w.setName(sc.nextLine().trim());
			return w;
		}
		throw new IOException("Unable to parse: " + s);
		// TODO LOW: Support show/prio?
	}

	public String emit(ArrayList<Exercise> allExercises) {
		String out = "";
		for (Exercise e : exercises) {
			int index = allExercises.indexOf(e);
			if (index != -1) {
				out += index + " ";
			}
		}
		out += name.replace('\n', ' ');
		return out;
		// TODO LOW: Support show/prio?
	}
}
