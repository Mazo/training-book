package train.book.data;

import train.book.Const;

public class ExerciseSet implements Comparable<ExerciseSet> {
	private long d1;
	private double d2;
	private long id = -1;
	private long setId = -1;
	private ExerciseType t;

	public ExerciseSet(long d1, double d2, ExerciseType t) {
		this.d1 = d1;
		this.d2 = d2;
		this.t = t;
	}

	public long getD1() {
		return d1;
	}

	public double getD2() {
		return d2;
	}

	public void setD1(long d1) {
		this.d1 = d1;
	}

	public void setD2(double d2) {
		this.d2 = d2;
	}

	@Override
	public String toString() {
		String tD1 = toStringD1();
		String tD2 = toStringD2();
		if (tD2.equals("")) {
			return tD1;
		} else {
			return tD1 + " " + tD2;
		}
	}

	private String toStringD1() {
		switch (t) {
		case DISTANCE_TIME:
			return d1 + " " + Const.TIME_UNIT;
		case REPS:
			return d1 + " x";
		case TIME:
			return d1 + " " + Const.TIME_UNIT;
		case WEIGHT_REPS:
			return d1 + " x";
		case UNKNOWN:
			return d1 + " ?";
		default:
			return "";
		}
	}

	private String toStringD2() {
		switch (t) {
		case DISTANCE_TIME:
			return d2 + " " + Const.DISTANCE_UNIT;
		case WEIGHT_REPS:
			return d2 + " " + Const.WEIGHT_UNIT;
		case UNKNOWN:
			return d2 + " ?";
		default:
			return "";
		}
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setSetId(long setId) {
		this.setId = setId;
	}

	public long getSetId() {
		return setId;
	}

	public ExerciseSet getCopy() {
		ExerciseSet copy = new ExerciseSet(d1, d2, t);
		copy.setId(id);
		copy.setSetId(setId);
		return copy;
	}

	public int compareTo(ExerciseSet another) {
		int cmp1 = t.compareTo(another.t);
		if (cmp1 != 0) {
			return cmp1;
		}
		int cmp2 = new Long(d1).compareTo(another.d1);
		if (cmp2 != 0) {
			return cmp2;
		}
		int cmp3 = new Double(d2).compareTo(another.d2);
		if (cmp3 != 0) {
			return cmp3;
		}
		int cmp4 = new Long(id).compareTo(another.id);
		if (cmp4 != 0) {
			return cmp4;
		}
		return new Long(setId).compareTo(another.setId);
	}
}
