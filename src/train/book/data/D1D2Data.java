package train.book.data;

public interface D1D2Data {
	public void setD1Value(long d1);

	public void setD2Value(double d2);

	public long getD1Value();

	public double getD2Value();
}
