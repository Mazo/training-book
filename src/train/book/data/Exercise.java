package train.book.data;

import java.io.IOException;
import java.util.Scanner;

public class Exercise implements Comparable<Exercise>, EditData {
	private String name;
	private ExerciseType type;
	private long id = -1;
	private Exercise backup = null;

	public Exercise(String name, ExerciseType type) {
		this.name = name;
		this.type = type;
	}

	public Exercise(String name) {
		this(name, ExerciseType.WEIGHT_REPS);
	}

	@Override
	public String toString() {
		return name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ExerciseType getType() {
		return type;
	}

	public void setType(ExerciseType type) {
		this.type = type;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	private Exercise getCopy() {
		Exercise c = new Exercise(name, type);
		c.id = id;
		return c;
	}

	public int compareTo(Exercise another) {
		int cmp1 = name.compareTo(another.name);
		if (cmp1 != 0) {
			return cmp1;
		}
		int cmp2 = new Long(id).compareTo(another.id);
		if (cmp2 != 0) {
			return cmp2;
		}
		return type.compareTo(another.type);
	}

	public void commit() {
		if (backup != null) {
			if (id == -1) {
				// New
			} else if (compareTo(backup) != 0) {
				// Changed
			}
			backup = null;
		}
	}

	public void revert() {
		if (backup != null) {
			name = backup.name;
			type = backup.type;
			id = backup.id;
			backup = null;
		}
	}

	public void startEditing() {
		if (backup == null) {
			backup = getCopy();
		}
	}

	public static Exercise parse(String s) throws IOException {
		Scanner sc = new Scanner(s);
		ExerciseType t = ExerciseType.WEIGHT_REPS;
		if (sc.hasNextInt()) {
			t = ExerciseType.getType(sc.nextInt());
		}
		if (sc.hasNextLine()) {
			return new Exercise(sc.nextLine().trim(), t);
		}
		throw new IOException("Unable to parse: " + s);
	}

	public String emit() {
		return type.getValue() + " " + name.replace('\n', ' ');
	}
}
