package train.book.data;

public enum ExerciseType {
	WEIGHT_REPS, REPS, DISTANCE_TIME, TIME, UNKNOWN;
	public int getValue() {
		switch (this) {
		case WEIGHT_REPS:
			return 1;
		case REPS:
			return 2;
		case DISTANCE_TIME:
			return 3;
		case TIME:
			return 4;
		default:
			return -1;
		}
	}

	public static ExerciseType getType(int value) {
		switch (value) {
		case 1:
			return WEIGHT_REPS;
		case 2:
			return REPS;
		case 3:
			return DISTANCE_TIME;
		case 4:
			return TIME;
		default:
			return UNKNOWN;
		}
	}
}
