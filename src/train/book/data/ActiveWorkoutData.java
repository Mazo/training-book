package train.book.data;

import java.util.Date;

public class ActiveWorkoutData extends WorkoutData {
	private int size;

	public ActiveWorkoutData(Workout w, Date startDate) {
		super(w.getId(), startDate);
		size = w.getNumberOfExercises();
		for (int i = 0; i < size; i++) {
			Exercise e = w.getExercise(i);
			addData(new ActiveExerciseData(this, e.getId(), e.getType(),
					e.getName()));
		}
	}

	@Override
	public ActiveExerciseData get(int i) {
		return (ActiveExerciseData) super.get(i);
	}

	public boolean isLast(int index) {
		return index + 1 == size;
	}

	public boolean hasNext(int index) {
		return index + 1 <= size;
	}
}
