package train.book;

import train.book.data.Exercise;

import android.content.Context;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

public class ExerciseSelectAdapter extends
		ArrayAdapter<Pair<Exercise, Boolean>> {

	public ExerciseSelectAdapter(Context context) {
		super(context, R.layout.checkbox_list_item);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = View.inflate(getContext(),
					R.layout.checkbox_list_item, null);
		}
		Pair<Exercise, Boolean> p = getItem(position);
		Exercise e = p.first;
		if (p != null) {
			CheckBox checkBox = (CheckBox) convertView
					.findViewById(R.id.checkBoxListItem);
			checkBox.setText(e.getName());
			checkBox.setChecked(p.second);
		}
		return convertView;
	}
}
