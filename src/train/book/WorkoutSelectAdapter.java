package train.book;

import java.util.List;

import train.book.data.Workout;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

public class WorkoutSelectAdapter extends ArrayAdapter<Workout> {

	public WorkoutSelectAdapter(Context context, List<Workout> list) {
		super(context, R.layout.checkbox_list_item, list);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = View.inflate(getContext(),
					R.layout.checkbox_list_item, null);
		}
		Workout w = getItem(position);
		if (w != null) {
			CheckBox checkBox = (CheckBox) convertView
					.findViewById(R.id.checkBoxListItem);
			checkBox.setText(w.getName());
			checkBox.setChecked(w.doShow());
		}
		return convertView;
	}
}
