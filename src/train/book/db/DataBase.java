package train.book.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DataBase {
	private WorkoutDataOpenHelper dbHelper;
	protected SQLiteDatabase db;

	public DataBase(Context context) {
		dbHelper = new WorkoutDataOpenHelper(context);
	}

	public void open() throws SQLException {
		db = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public SQLiteDatabase DB() {
		return db;
	}

	// TODO LOW: Remove unused methods in DB's + make them safer e.g. remove
	// when taking long and replace with .getId for example.
}
