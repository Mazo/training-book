package train.book.db;

import java.util.ArrayList;

import train.book.data.Exercise;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class DBWorkoutExercise extends DB {

	private static final String TABLE_NAME = "work_exer";
	private static final String COLUMN_WORKOUT_ID = "work_id";
	private static final String COLUMN_EXERCISE_ID = "exer_id";

	private static final String CREATE_COMMAND = "CREATE TABLE " + TABLE_NAME
			+ "(" + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUMN_WORKOUT_ID + " INTEGER NOT NULL, " + COLUMN_EXERCISE_ID
			+ " INTEGER NOT NULL);";

	private static final String[] ALL_COLUMNS = { BaseColumns._ID,
			COLUMN_WORKOUT_ID, COLUMN_EXERCISE_ID };

	private static final String ORDER = BaseColumns._ID + " ASC";

	@Override
	protected String getCreateCommand() {
		return CREATE_COMMAND;
	}

	@Override
	protected String getTableName() {
		return TABLE_NAME;
	}

	private static ContentValues getContentValues(long workoutId,
			long exerciseId) {
		ContentValues values = new ContentValues();
		values.put(COLUMN_WORKOUT_ID, workoutId);
		values.put(COLUMN_EXERCISE_ID, exerciseId);
		return values;
	}

	public static ArrayList<Exercise> load(SQLiteDatabase db, long workoutId) {
		ArrayList<Exercise> result = new ArrayList<Exercise>();
		Cursor cursor = db.query(TABLE_NAME, ALL_COLUMNS, COLUMN_WORKOUT_ID
				+ "=?", new String[] { Long.toString(workoutId) }, null, null,
				ORDER);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			long exerciseId = cursor.getLong(2);

			Exercise e = DBExercise.load(db, exerciseId);
			if (e != null) {
				result.add(e);
			}
			cursor.moveToNext();
		}
		cursor.close();
		return result;
	}

	public static void store(SQLiteDatabase db, long workoutId, long exerciseId) {
		db.insert(TABLE_NAME, null, getContentValues(workoutId, exerciseId));
	}

	public static void delete(SQLiteDatabase db, long workoutId, long exerciseId) {
		db.delete(
				TABLE_NAME,
				COLUMN_WORKOUT_ID + "=? AND " + COLUMN_EXERCISE_ID + "=?",
				new String[] { Long.toString(workoutId),
						Long.toString(exerciseId) });
	}

	public static void delete(SQLiteDatabase db, long workoutId) {
		db.delete(TABLE_NAME, COLUMN_WORKOUT_ID + "=?",
				new String[] { Long.toString(workoutId) });
	}

	public static ArrayList<Exercise> loadShell(SQLiteDatabase db,
			long workoutId, ArrayList<Exercise> exerciseList) {
		ArrayList<Exercise> result = new ArrayList<Exercise>();
		Cursor cursor = db.query(TABLE_NAME, ALL_COLUMNS, COLUMN_WORKOUT_ID
				+ "=?", new String[] { Long.toString(workoutId) }, null, null,
				ORDER);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			long exerciseId = cursor.getLong(2);

			Exercise e = find(exerciseList, exerciseId);
			if (e != null) {
				result.add(e);
			}
			cursor.moveToNext();
		}
		cursor.close();
		return result;
	}

	private static Exercise find(ArrayList<Exercise> exerciseList,
			long exerciseId) {
		// TODO LOW: Improve performance.
		for (Exercise e : exerciseList) {
			if (e.getId() == exerciseId) {
				return e;
			}
		}
		return null;
	}
}
