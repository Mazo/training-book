package train.book.db;

import java.util.ArrayList;

import train.book.data.ExerciseData;
import train.book.data.ExerciseSet;
import train.book.data.ExerciseType;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class DBExerciseSetData extends DB {

	private static final String TABLE_NAME = "exercise_set";
	private static final String COLUMN_SET_ID = "set_id";
	private static final String COLUMN_DATA_1 = "data_1";
	private static final String COLUMN_DATA_2 = "data_2";

	private static final String CREATE_COMMAND = "CREATE TABLE " + TABLE_NAME
			+ "(" + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUMN_SET_ID + " INTEGER NOT NULL, " + COLUMN_DATA_1
			+ " INTEGER NOT NULL, " + COLUMN_DATA_2 + " REAL NOT NULL);";

	private static final String[] ALL_COLUMNS = { BaseColumns._ID,
			COLUMN_SET_ID, COLUMN_DATA_1, COLUMN_DATA_2 };

	private static final String ORDER = BaseColumns._ID + " ASC";

	@Override
	protected String getTableName() {
		return TABLE_NAME;
	}

	@Override
	protected String getCreateCommand() {
		return CREATE_COMMAND;
	}

	private static ContentValues getContentValues(ExerciseSet s) {
		ContentValues values = new ContentValues();
		values.put(COLUMN_SET_ID, s.getSetId());
		values.put(COLUMN_DATA_1, s.getD1());
		values.put(COLUMN_DATA_2, s.getD2());
		return values;
	}

	public static void store(SQLiteDatabase db, ExerciseSet s) {
		db.insert(TABLE_NAME, null, getContentValues(s));
	}

	public static ArrayList<ExerciseSet> load(SQLiteDatabase db,
			long inputSetId, ExerciseType t) {

		ArrayList<ExerciseSet> result = new ArrayList<ExerciseSet>();
		Cursor cursor = db.query(TABLE_NAME, ALL_COLUMNS, COLUMN_SET_ID + "=?",
				new String[] { Long.toString(inputSetId) }, null, null, ORDER);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			long id = cursor.getLong(0);
			long setId = cursor.getLong(1);
			long d1 = cursor.getLong(2);
			double d2 = cursor.getDouble(3);

			ExerciseSet s = new ExerciseSet(d1, d2, t);
			s.setId(id);
			s.setSetId(setId);
			result.add(s);

			cursor.moveToNext();
		}
		cursor.close();

		return result;
	}

	public static void delete(SQLiteDatabase db, ExerciseData d) {
		db.delete(TABLE_NAME, COLUMN_SET_ID + "=?",
				new String[] { Long.toString(d.getId()) });
	}
}
