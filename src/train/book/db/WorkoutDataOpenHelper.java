package train.book.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class WorkoutDataOpenHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "workouts.db";
	private static final int DATABASE_VERSION = 1;
	private ArrayList<DB> dbs;

	public WorkoutDataOpenHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);

		dbs = getDBs();
	}

	public static ArrayList<DB> getDBs() {
		ArrayList<DB> res = new ArrayList<DB>();
		res.add(new DBWorkout());
		res.add(new DBExercise());
		res.add(new DBWorkoutData());
		res.add(new DBWorkoutExercise());
		res.add(new DBExerciseData());
		res.add(new DBExerciseSetData());
		res.add(new DBWeightData());
		return res;
	}

	public static boolean exportDB(SQLiteDatabase db, File destination) {
		File oldDB = new File(db.getPath());
		try {
			FileInputStream fis = new FileInputStream(oldDB);
			FileChannel src = fis.getChannel();
			FileOutputStream fos = new FileOutputStream(destination);
			FileChannel dst = fos.getChannel();
			dst.transferFrom(src, 0, src.size());
			src.close();
			dst.close();
			fis.close();
			fos.close();
			return true;
		} catch (IOException e) {
			Log.e("DB", "export", e);
		}
		return false;
	}

	public static boolean importDB(SQLiteDatabase db, File source) {
		File newDB = new File(db.getPath());
		try {
			FileInputStream fis = new FileInputStream(source);
			FileChannel src = fis.getChannel();
			FileOutputStream fos = new FileOutputStream(newDB);
			FileChannel dst = fos.getChannel();
			dst.transferFrom(src, 0, src.size());
			src.close();
			dst.close();
			fis.close();
			fos.close();
			return true;
		} catch (IOException e) {
			Log.e("DB", "import", e);
		}
		return false;
	}

	public static void deleteAll(SQLiteDatabase db) {
		for (DB d : getDBs()) {
			d.deleteAll(db);
		}
		Log.w("DB", "Deleted all data");
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		for (DB d : dbs) {
			d.onCreate(db);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w("DB", "Upgrading database from version " + oldVersion + " to "
				+ newVersion + ", which will destroy all old data");

		for (DB d : dbs) {
			d.onUpdate(db);
		}
		// TODO LOW: Maybe not recreate it?
		onCreate(db);
	}

}
