package train.book.db;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import train.book.activities.TrainbookActivity;
import train.book.data.Exercise;
import train.book.data.WeightData;
import train.book.data.Workout;
import train.book.data.WorkoutData;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

public class Backup {
	public static final String RAW = ".tbbak";
	public static final String DB = ".dbbak";
	private static final String FILE_NAME_RAW = "training_book" + RAW;
	private static final String FILE_NAME_DB = "training_book" + DB;

	public static File createData(Context c, SQLiteDatabase db) {
		String state = Environment.getExternalStorageState();
		// Make sure it is writable
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			File file = new File(c.getExternalFilesDir(null), FILE_NAME_DB);

			boolean exported = WorkoutDataOpenHelper.exportDB(db, file);
			if (exported) {
				return file;
			}
		}
		// TODO LOW: Handle error better?
		return null;
	}

	public static void importData(File file, SQLiteDatabase db) {
		boolean imported = WorkoutDataOpenHelper.importDB(db, file);
		if (!imported) {
			// TODO LOW: Error?
		}
	}

	public static File createSlow(Context c, SQLiteDatabase db) {
		String state = Environment.getExternalStorageState();
		// Make sure it is writable
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			File file = new File(c.getExternalFilesDir(null), FILE_NAME_RAW);

			try {
				ArrayList<Exercise> exercises = TrainbookActivity
						.getExerciseList();
				ArrayList<Workout> workouts = TrainbookActivity
						.getWorkoutList();
				ArrayList<WorkoutData> workoutHistory = DBWorkoutData.load(db);
				ArrayList<WeightData> weightHistory = DBWeightData.load(db);

				FileWriter out = new FileWriter(file);
				for (Exercise e : exercises) {
					out.write(e.emit());
					out.write('\n');
				}
				out.write('\n');
				for (Workout w : workouts) {
					out.write(w.emit(exercises));
					out.write('\n');
				}
				out.write('\n');
				for (WorkoutData d : workoutHistory) {
					out.write(d.emit(workouts, exercises));
					out.write('\n');
				}
				out.write('\n');
				for (WeightData d : weightHistory) {
					out.write(d.emit());
					out.write('\n');
				}
				out.write('\n');

				out.close();
				return file;
			} catch (IOException e) {
				Log.w("Backup", "Error exporting " + file, e);
			} catch (IndexOutOfBoundsException e) {
				Log.w("Backup", "Error exporting " + file, e);
			}
		}
		// TODO LOW: Handle error better?
		return null;
	}

	public static void importSlow(File file, SQLiteDatabase db) {
		// TODO LOW: Maybe file.canRead()?
		try {
			Scanner in = new Scanner(file);

			ArrayList<Exercise> exercises = new ArrayList<Exercise>();
			ArrayList<Workout> workouts = new ArrayList<Workout>();

			Log.w("Loading", "exercises");
			// Get exercises
			while (in.hasNextLine()) {
				String line = in.nextLine().trim();
				if (line.length() == 0) {
					break;
				}
				Exercise e = Exercise.parse(line);
				exercises.add(e);
			}
			Log.w("Storing", "exercises");
			for (Exercise e : exercises) {
				DBExercise.store(db, e);
			}

			Log.w("Loading", "workouts");
			// Get workouts
			while (in.hasNextLine()) {
				String line = in.nextLine().trim();
				if (line.length() == 0) {
					break;
				}
				Workout w = Workout.parse(line, exercises);
				workouts.add(w);
			}
			Log.w("Storing", "workouts");
			for (Workout w : workouts) {
				DBWorkout.store(db, w);
			}

			Log.w("Loading", "history");
			// Get workout history
			while (in.hasNextLine()) {
				String line = in.nextLine().trim();
				if (line.length() == 0) {
					break;
				}
				WorkoutData d = WorkoutData.parse(line, workouts, exercises);
				DBWorkoutData.store(db, d);
			}

			Log.w("Loading", "weight");
			// Get weight history
			while (in.hasNextLine()) {
				String line = in.nextLine().trim();
				if (line.length() == 0) {
					break;
				}
				WeightData d = WeightData.parse(line);
				DBWeightData.store(db, d);
			}

			in.close();
			Log.w("Done", "!!!");
		} catch (IOException e) {
			Log.w("Backup", "Error importing " + file, e);
		} catch (IndexOutOfBoundsException e) {
			Log.w("Backup", "Error importing " + file, e);
		}
	}

	public static void deleteSlow(Context c) {
		File file = new File(c.getExternalFilesDir(null), FILE_NAME_RAW);
		if (file != null) {
			file.delete();
		}
	}

	public static void deleteData(Context c) {
		File file = new File(c.getExternalFilesDir(null), FILE_NAME_DB);
		if (file != null) {
			file.delete();
		}
	}
}
