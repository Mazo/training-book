package train.book.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

public class DataBaseHandler {
	private static DataBaseHandler instance = null;
	private static boolean closing = false;
	private Handler handler;
	private DBThread dbThread;
	private DataBase db;

	private DataBaseHandler(Context context) {
		db = new DataBase(context);
		dbThread = new DBThread();
		dbThread.start();
		handler = new Handler(dbThread.getLooper());
	}

	public static synchronized void init(Context context) {
		if (instance == null) {
			instance = new DataBaseHandler(context);
			closing = false;
		}
	}

	public static synchronized void post(DBRunnable r) {
		if (instance != null) {
			instance.handler.post(r);
		} else {
			Log.e(DataBaseHandler.class.getSimpleName(),
					"need to call init first");
		}
	}

	public static synchronized void quit() {
		if (instance != null) {
			closing = true;
			instance.dbThread.quit();

			try {
				instance.dbThread.join();
			} catch (InterruptedException e) {
			}
			instance.db.close();
			instance = null;
		}
	}

	public static boolean isClosing() {
		return closing;
	}

	public static abstract class DBRunnable implements Runnable {

		public abstract void run(SQLiteDatabase db);

		public final void run() {
			if (instance != null) {
				instance.db.open();
				run(instance.db.DB());
				if (instance != null) {
					instance.db.close();
				}
			}
		}
	}

	private class DBThread extends HandlerThread {
		public DBThread() {
			super("DataBaseThread");
		}
	}
}
