package train.book.db;

import java.util.ArrayList;

import train.book.data.Exercise;
import train.book.data.ExerciseData;
import train.book.data.ExerciseSet;
import train.book.data.Workout;
import train.book.data.WorkoutData;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class DBExerciseData extends DB {

	private static final String TABLE_NAME = "exercise_data";
	private static final String COLUMN_WORKOUT_ID = "work_id";
	private static final String COLUMN_EXERCISE_ID = "exer_id";

	private static final String CREATE_COMMAND = "CREATE TABLE " + TABLE_NAME
			+ "(" + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUMN_WORKOUT_ID + " INTEGER NOT NULL, " + COLUMN_EXERCISE_ID
			+ " INTEGER NOT NULL);";

	private static final String[] ALL_COLUMNS = { BaseColumns._ID,
			COLUMN_WORKOUT_ID, COLUMN_EXERCISE_ID };

	private static final String ORDER = BaseColumns._ID + " ASC";

	@Override
	protected String getTableName() {
		return TABLE_NAME;
	}

	@Override
	protected String getCreateCommand() {
		return CREATE_COMMAND;
	}

	private static ContentValues getContentValues(long workoutId,
			long exerciseId) {
		ContentValues values = new ContentValues();
		values.put(COLUMN_WORKOUT_ID, workoutId);
		values.put(COLUMN_EXERCISE_ID, exerciseId);
		return values;
	}

	public static void store(SQLiteDatabase db, ExerciseData exerciseData,
			long workoutId) {
		long exerciseId = exerciseData.getExerciseId();
		long id = db.insert(TABLE_NAME, null,
				getContentValues(workoutId, exerciseId));
		int size = exerciseData.size();
		for (int i = 0; i < size; i++) {
			ExerciseSet s = exerciseData.get(i);
			s.setSetId(id);
			DBExerciseSetData.store(db, s);
		}
	}

	public static ExerciseData load(SQLiteDatabase db, long id) {
		ArrayList<ExerciseData> res = load(db, BaseColumns._ID + "=?",
				new String[] { Long.toString(id) }, ORDER);
		if (res.size() == 0) {
			return null;
		} else {
			return res.get(0);
		}
	}

	public static ArrayList<ExerciseData> load(SQLiteDatabase db, WorkoutData d) {
		return load(db, COLUMN_WORKOUT_ID + "=?",
				new String[] { Long.toString(d.getId()) }, ORDER);
	}

	private static ArrayList<ExerciseData> load(SQLiteDatabase db,
			String selection, String[] selectionArgs, String order) {
		ArrayList<ExerciseData> result = new ArrayList<ExerciseData>();
		Cursor cursor = db.query(TABLE_NAME, ALL_COLUMNS, selection,
				selectionArgs, null, null, order);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			long id = cursor.getLong(0);
			long exerciseId = cursor.getLong(2);

			ExerciseData d = new ExerciseData(exerciseId);
			d.setId(id);

			Exercise e = DBExercise.load(db, exerciseId);

			if (e != null) {
				d.setName(e.getName());
				d.setType(e.getType());
			}

			ArrayList<ExerciseSet> res = DBExerciseSetData.load(db, id,
					d.getType());
			for (ExerciseSet s : res) {
				d.addSet(s);
			}
			result.add(d);

			cursor.moveToNext();
		}
		cursor.close();

		return result;
	}

	public static void update(SQLiteDatabase db, ExerciseData d) {
		DBExerciseSetData.delete(db, d);
		for (int i = 0; i < d.size(); i++) {
			DBExerciseSetData.store(db, d.get(i));
		}
	}

	public static void delete(SQLiteDatabase db, ExerciseData d) {
		db.delete(TABLE_NAME, BaseColumns._ID + "=?",
				new String[] { Long.toString(d.getId()) });
		DBExerciseSetData.delete(db, d);
	}

	public static ExerciseData getPrevious(SQLiteDatabase db, long exerciseId) {
		ArrayList<ExerciseData> res = load(db, COLUMN_EXERCISE_ID + "=?",
				new String[] { Long.toString(exerciseId) }, COLUMN_WORKOUT_ID
						+ " DESC" + " LIMIT 1");
		// TODO LOW: The highest workout id isn't always the most recent one
		// (when its date is changed for instance)
		if (res.size() == 0) {
			return null;
		} else {
			return res.get(0);
		}
	}

	public static ArrayList<ExerciseData> getPrevious(SQLiteDatabase db,
			Workout w) {
		ArrayList<ExerciseData> res = new ArrayList<ExerciseData>();
		for (int i = 0; i < w.getNumberOfExercises(); i++) {
			ExerciseData e = getPrevious(db, w.getExercise(i).getId());
			if (e != null) {
				res.add(e);
			}
		}
		return res;
	}
}
