package train.book.db;

import java.util.ArrayList;

import train.book.data.Exercise;
import train.book.data.ExerciseType;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class DBExercise extends DB {

	private static final String TABLE_NAME = "exercises";
	private static final String COLUMN_NAME = "name";
	private static final String COLUMN_TYPE = "type";

	private static final String CREATE_COMMAND = "CREATE TABLE " + TABLE_NAME
			+ "(" + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUMN_NAME + " TEXT NOT NULL, " + COLUMN_TYPE
			+ " INTEGER NOT NULL);";

	private static final String[] ALL_COLUMNS = { BaseColumns._ID, COLUMN_NAME,
			COLUMN_TYPE };

	private static final String ORDER = COLUMN_NAME + " ASC, "
			+ BaseColumns._ID + " ASC";

	@Override
	protected String getTableName() {
		return TABLE_NAME;
	}

	@Override
	protected String getCreateCommand() {
		return CREATE_COMMAND;
	}

	private static ContentValues getContentValues(Exercise e) {
		ContentValues values = new ContentValues();
		values.put(COLUMN_NAME, e.getName());
		values.put(COLUMN_TYPE, e.getType().getValue());
		return values;
	}

	public static ArrayList<Exercise> load(SQLiteDatabase db) {
		return load(db, null, null);
	}

	public static Exercise load(SQLiteDatabase db, long id) {
		ArrayList<Exercise> res = load(db, BaseColumns._ID + "=?",
				new String[] { Long.toString(id) });
		if (res.size() == 0) {
			return null;
		} else {
			return res.get(0);
		}
	}

	private static ArrayList<Exercise> load(SQLiteDatabase db,
			String selection, String[] selectionArgs) {
		ArrayList<Exercise> result = new ArrayList<Exercise>();
		Cursor cursor = db.query(TABLE_NAME, ALL_COLUMNS, selection,
				selectionArgs, null, null, ORDER);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			long id = cursor.getLong(0);
			String name = cursor.getString(1);
			ExerciseType type = ExerciseType.getType(cursor.getInt(2));

			Exercise e = new Exercise(name, type);
			e.setId(id);

			result.add(e);
			cursor.moveToNext();
		}

		cursor.close();
		return result;
	}

	public static void store(SQLiteDatabase db, Exercise e) {
		long id = db.insert(TABLE_NAME, null, getContentValues(e));
		e.setId(id);
	}

	public static void update(SQLiteDatabase db, Exercise e) {
		db.update(TABLE_NAME, getContentValues(e), BaseColumns._ID + "=?",
				new String[] { Long.toString(e.getId()) });
	}

	public static void delete(SQLiteDatabase db, Exercise e) {
		db.delete(TABLE_NAME, BaseColumns._ID + "=?",
				new String[] { Long.toString(e.getId()) });
	}
}
