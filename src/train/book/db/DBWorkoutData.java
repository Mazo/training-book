package train.book.db;

import java.util.ArrayList;
import java.util.Date;

import train.book.data.ExerciseData;
import train.book.data.Workout;
import train.book.data.WorkoutData;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Pair;

public class DBWorkoutData extends DB {

	private static final String TABLE_NAME = "workout_data";
	private static final String COLUMN_DATE = "date";
	private static final String COLUMN_WORKOUT_ID = "workout";
	private static final String COLUMN_COMMENT = "comment";

	private static final String CREATE_COMMAND = "CREATE TABLE " + TABLE_NAME
			+ "(" + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUMN_DATE + " INTEGER NOT NULL, " + COLUMN_WORKOUT_ID
			+ " INTEGER NOT NULL, " + COLUMN_COMMENT + " TEXT NOT NULL);";

	private static final String[] ALL_COLUMNS = { BaseColumns._ID, COLUMN_DATE,
			COLUMN_WORKOUT_ID, COLUMN_COMMENT };

	private static final String ORDER = COLUMN_DATE + " DESC, "
			+ BaseColumns._ID + " ASC";

	@Override
	protected String getTableName() {
		return TABLE_NAME;
	}

	@Override
	protected String getCreateCommand() {
		return CREATE_COMMAND;
	}

	private static ContentValues getContentValues(WorkoutData data) {
		ContentValues values = new ContentValues();
		values.put(COLUMN_DATE, data.getTime());
		values.put(COLUMN_WORKOUT_ID, data.getWorkoutId());
		values.put(COLUMN_COMMENT, data.getComment());
		return values;
	}

	public static void store(SQLiteDatabase db, WorkoutData data) {
		long id = db.insert(TABLE_NAME, null, getContentValues(data));
		data.setId(id);

		for (int i = 0; i < data.size(); i++) {
			DBExerciseData.store(db, data.get(i), id);
		}
	}

	public static ArrayList<WorkoutData> loadX(SQLiteDatabase db, int x,
			int offset) {
		return load(db, null, null, ORDER + " LIMIT " + x + " OFFSET " + offset);
	}

	public static ArrayList<WorkoutData> load(SQLiteDatabase db) {
		return load(db, null, null, ORDER);
	}

	public static WorkoutData load(SQLiteDatabase db, long id) {
		ArrayList<WorkoutData> res = load(db, BaseColumns._ID + "=?",
				new String[] { Long.toString(id) }, null);
		if (res.size() == 0) {
			return null;
		} else {
			return res.get(0);
		}
	}

	private static ArrayList<WorkoutData> load(SQLiteDatabase db,
			String selection, String[] selectionArgs, String order) {
		ArrayList<WorkoutData> result = new ArrayList<WorkoutData>();

		Cursor cursor = db.query(TABLE_NAME, ALL_COLUMNS, selection,
				selectionArgs, null, null, order);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			long id = cursor.getLong(0);
			Date date = new Date(cursor.getLong(1));
			long workoutId = cursor.getLong(2);
			String comment = cursor.getString(3);

			WorkoutData d = new WorkoutData(workoutId, date);

			Workout w = DBWorkout.load(db, workoutId);
			if (w != null) {
				d.setName(w.getName());
			}
			d.setId(id);
			d.setComment(comment);

			ArrayList<ExerciseData> res = DBExerciseData.load(db, d);
			for (ExerciseData e : res) {
				d.addData(e);
			}
			result.add(d);

			cursor.moveToNext();
		}
		cursor.close();

		return result;
	}

	public static void delete(SQLiteDatabase db, WorkoutData d) {
		db.delete(TABLE_NAME, BaseColumns._ID + "=?",
				new String[] { Long.toString(d.getId()) });
		for (int i = 0; i < d.size(); i++) {
			DBExerciseData.delete(db, d.get(i));
		}
	}

	public static void update(SQLiteDatabase db, WorkoutData d) {
		db.update(TABLE_NAME, getContentValues(d), BaseColumns._ID + "=?",
				new String[] { Long.toString(d.getId()) });

		ArrayList<ExerciseData> res = DBExerciseData.load(db, d);
		for (ExerciseData tmp : res) {
			DBExerciseData.delete(db, tmp);
		}
		for (int i = 0; i < d.size(); i++) {
			DBExerciseData.store(db, d.get(i), d.getId());
		}
	}

	public static ExerciseData getExerciseData(SQLiteDatabase db,
			long beforeTime, long workoutId, long exerciseId) {

		ArrayList<WorkoutData> res = load(db, COLUMN_WORKOUT_ID + "=? AND "
				+ COLUMN_DATE + "<?", new String[] { Long.toString(workoutId),
				Long.toString(beforeTime) }, ORDER + " LIMIT 1");
		if (res.size() != 0) {
			WorkoutData d = res.get(0);
			for (int i = 0; i < d.size(); i++) {
				ExerciseData tmp = d.get(i);
				if (tmp.getExerciseId() == exerciseId) {
					return tmp;
				}
			}
		}
		return new ExerciseData(exerciseId);
	}

	public static ArrayList<Pair<Workout, Long>> getLatestTimes(
			SQLiteDatabase db, ArrayList<Workout> workoutShellList) {
		ArrayList<Pair<Workout, Long>> result = new ArrayList<Pair<Workout, Long>>();

		for (Workout w : workoutShellList) {
			long date = getLatestTime(db, w.getId());
			result.add(new Pair<Workout, Long>(w, date));
		}

		return result;
	}

	public static long getLatestTime(SQLiteDatabase db, long workoutId) {
		Cursor cursor = db.query(TABLE_NAME, new String[] { COLUMN_DATE },
				COLUMN_WORKOUT_ID + "=?",
				new String[] { Long.toString(workoutId) }, null, null, ORDER
						+ " LIMIT 1");

		long date = 0;
		if (cursor.moveToFirst()) {
			date = cursor.getLong(0);
		}
		cursor.close();
		return date;
	}

	public static WorkoutData getPrevious(SQLiteDatabase db, Workout w) {
		ArrayList<WorkoutData> res = load(db, COLUMN_WORKOUT_ID + "=?",
				new String[] { Long.toString(w.getId()) }, ORDER + " LIMIT 1");
		if (res.size() == 0) {
			return null;
		} else {
			return res.get(0);
		}
	}
}
