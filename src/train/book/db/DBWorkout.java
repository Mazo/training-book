package train.book.db;

import java.util.ArrayList;

import train.book.R;
import train.book.data.Exercise;
import train.book.data.ExerciseType;
import train.book.data.Workout;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class DBWorkout extends DB {

	private static final String TABLE_NAME = "workouts";
	private static final String COLUMN_PRIO = "prio";
	private static final String COLUMN_NAME = "name";
	private static final String COLUMN_SHOW = "show";

	private static final String CREATE_COMMAND = "CREATE TABLE " + TABLE_NAME
			+ "(" + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUMN_PRIO + " INTEGER NOT NULL, " + COLUMN_NAME
			+ " TEXT NOT NULL," + COLUMN_SHOW + " INTEGER NOT NULL);";

	private static final String[] ALL_COLUMNS = { BaseColumns._ID, COLUMN_PRIO,
			COLUMN_NAME, COLUMN_SHOW };

	private static final String ORDER = COLUMN_PRIO + " ASC, "
			+ BaseColumns._ID + " ASC";

	@Override
	protected String getTableName() {
		return TABLE_NAME;
	}

	@Override
	protected String getCreateCommand() {
		return CREATE_COMMAND;
	}

	private static ContentValues getContentValues(Workout w) {
		ContentValues values = new ContentValues();
		values.put(COLUMN_PRIO, w.getPrio());
		values.put(COLUMN_NAME, w.getName());
		values.put(COLUMN_SHOW, w.doShow());
		return values;
	}

	public static Workout load(SQLiteDatabase db, long id) {
		ArrayList<Workout> res = load(db, BaseColumns._ID + "=?",
				new String[] { Long.toString(id) });
		if (res.size() == 0) {
			return null;
		} else {
			return res.get(0);
		}
	}

	public static ArrayList<Workout> loadShell(SQLiteDatabase db,
			ArrayList<Exercise> exerciseList) {
		ArrayList<Workout> result = new ArrayList<Workout>();
		Cursor cursor = db.query(TABLE_NAME, ALL_COLUMNS, null, null, null,
				null, ORDER);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			long id = cursor.getLong(0);
			long prio = cursor.getLong(1);
			String name = cursor.getString(2);
			boolean show = cursor.getInt(3) == 1 ? true : false;

			Workout w = new Workout(name);
			w.setId(id);
			w.setPrio(prio);
			w.setShow(show);

			ArrayList<Exercise> tmp = DBWorkoutExercise.loadShell(db,
					w.getId(), exerciseList);
			for (Exercise e : tmp) {
				w.addExercise(e);
			}

			result.add(w);
			cursor.moveToNext();
		}

		cursor.close();
		return result;
	}

	private static ArrayList<Workout> load(SQLiteDatabase db, String selection,
			String[] selectionArgs) {
		ArrayList<Workout> result = new ArrayList<Workout>();
		Cursor cursor = db.query(TABLE_NAME, ALL_COLUMNS, selection,
				selectionArgs, null, null, ORDER);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			long id = cursor.getLong(0);
			long prio = cursor.getLong(1);
			String name = cursor.getString(2);
			boolean show = cursor.getInt(3) == 1 ? true : false;

			Workout w = new Workout(name);
			w.setId(id);
			w.setPrio(prio);
			w.setShow(show);

			ArrayList<Exercise> tmp = DBWorkoutExercise.load(db, w.getId());
			for (Exercise e : tmp) {
				w.addExercise(e);
			}

			result.add(w);
			cursor.moveToNext();
		}

		cursor.close();
		return result;
	}

	public static void store(SQLiteDatabase db, Workout w) {
		long id = db.insert(TABLE_NAME, null, getContentValues(w));
		w.setId(id);

		for (int i = 0; i < w.getNumberOfExercises(); i++) {
			DBWorkoutExercise.store(db, id, w.getExercise(i).getId());
		}
	}

	public static void update(SQLiteDatabase db, Workout w) {
		db.update(TABLE_NAME, getContentValues(w), BaseColumns._ID + "=?",
				new String[] { Long.toString(w.getId()) });
		DBWorkoutExercise.delete(db, w.getId());
		for (int i = 0; i < w.getNumberOfExercises(); i++) {
			DBWorkoutExercise.store(db, w.getId(), w.getExercise(i).getId());
		}
		// TODO LOW: Improve all updates?
	}

	public static void delete(SQLiteDatabase db, Workout w) {
		db.delete(TABLE_NAME, BaseColumns._ID + "=?",
				new String[] { Long.toString(w.getId()) });
		DBWorkoutExercise.delete(db, w.getId());
	}

	@SuppressWarnings("unused")
	private static void storeDefautWorkouts(SQLiteDatabase db, Context c) {
		Workout chest = new Workout(c.getString(R.string.default_workout_chest));
		chest.addExercise(new Exercise(c
				.getString(R.string.default_exercise_bench),
				ExerciseType.WEIGHT_REPS));
		chest.addExercise(new Exercise(c
				.getString(R.string.default_exercise_dumb_press),
				ExerciseType.WEIGHT_REPS));
		chest.addExercise(new Exercise(c
				.getString(R.string.default_exercise_peckdeck),
				ExerciseType.WEIGHT_REPS));
		chest.addExercise(new Exercise(c
				.getString(R.string.default_exercise_cable_cross),
				ExerciseType.WEIGHT_REPS));
		chest.addExercise(new Exercise(c
				.getString(R.string.default_exercise_cable_drag),
				ExerciseType.WEIGHT_REPS));

		Workout absShoulder = new Workout(
				c.getString(R.string.default_workout_abs_shoulder));
		absShoulder.addExercise(new Exercise(c
				.getString(R.string.default_exercise_shoulder_press),
				ExerciseType.WEIGHT_REPS));
		absShoulder.addExercise(new Exercise(c
				.getString(R.string.default_exercise_dumb_fly),
				ExerciseType.WEIGHT_REPS));
		absShoulder.addExercise(new Exercise(c
				.getString(R.string.default_exercise_incl_situps),
				ExerciseType.WEIGHT_REPS));
		absShoulder.addExercise(new Exercise(c
				.getString(R.string.default_exercise_shoulder_rotation),
				ExerciseType.WEIGHT_REPS));
		absShoulder.addExercise(new Exercise(c
				.getString(R.string.default_exercise_laying_shoulder),
				ExerciseType.WEIGHT_REPS));
		absShoulder.addExercise(new Exercise(c
				.getString(R.string.default_exercise_crunches),
				ExerciseType.WEIGHT_REPS));

		Workout bicepsTri = new Workout(
				c.getString(R.string.default_workout_biceps_triceps));
		bicepsTri.addExercise(new Exercise(c
				.getString(R.string.default_exercise_bench_small_grip),
				ExerciseType.WEIGHT_REPS));
		bicepsTri.addExercise(new Exercise(c
				.getString(R.string.default_exercise_barb_curl),
				ExerciseType.WEIGHT_REPS));
		bicepsTri.addExercise(new Exercise(c
				.getString(R.string.default_exercise_hammer_curl),
				ExerciseType.WEIGHT_REPS));
		bicepsTri.addExercise(new Exercise(c
				.getString(R.string.default_exercise_laying_triceps),
				ExerciseType.WEIGHT_REPS));
		bicepsTri.addExercise(new Exercise(c
				.getString(R.string.default_exercise_conc_curl),
				ExerciseType.WEIGHT_REPS));
		bicepsTri.addExercise(new Exercise(c
				.getString(R.string.default_exercise_triceps_rope),
				ExerciseType.WEIGHT_REPS));

		Workout absBack = new Workout(
				c.getString(R.string.default_workout_abs_back));
		absBack.addExercise(new Exercise(c
				.getString(R.string.default_exercise_chinups),
				ExerciseType.WEIGHT_REPS));
		absBack.addExercise(new Exercise(c
				.getString(R.string.default_exercise_bale_rotation),
				ExerciseType.WEIGHT_REPS));
		absBack.addExercise(new Exercise(c
				.getString(R.string.default_exercise_leg_lift),
				ExerciseType.WEIGHT_REPS));
		absBack.addExercise(new Exercise(c
				.getString(R.string.default_exercise_rowing),
				ExerciseType.WEIGHT_REPS));
		absBack.addExercise(new Exercise(c
				.getString(R.string.default_exercise_back_ext),
				ExerciseType.WEIGHT_REPS));
		absBack.addExercise(new Exercise(c
				.getString(R.string.default_exercise_tens_load),
				ExerciseType.WEIGHT_REPS));

		storeFull(db, chest);
		storeFull(db, absShoulder);
		storeFull(db, bicepsTri);
		storeFull(db, absBack);
	}

	private static void storeFull(SQLiteDatabase db, Workout w) {
		for (int i = 0; i < w.getNumberOfExercises(); i++) {
			DBExercise.store(db, w.getExercise(i));
		}
		store(db, w);
	}
}
