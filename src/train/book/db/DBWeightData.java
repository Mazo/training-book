package train.book.db;

import java.util.ArrayList;
import java.util.Date;

import train.book.Const;
import train.book.data.WeightData;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class DBWeightData extends DB {

	private static final String TABLE_NAME = "weight_data";
	private static final String COLUMN_DATE = "date";
	private static final String COLUMN_WEIGHT = "weight";

	private static final String CREATE_COMMAND = "CREATE TABLE " + TABLE_NAME
			+ "(" + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUMN_DATE + " INTEGER NOT NULL, " + COLUMN_WEIGHT
			+ " REAL NOT NULL);";

	private static final String[] ALL_COLUMNS = { BaseColumns._ID, COLUMN_DATE,
			COLUMN_WEIGHT };

	private static final String ORDER = COLUMN_DATE + " DESC, "
			+ BaseColumns._ID + " ASC";

	@Override
	protected String getTableName() {
		return TABLE_NAME;
	}

	@Override
	protected String getCreateCommand() {
		return CREATE_COMMAND;
	}

	private static ContentValues getContentValues(WeightData data) {
		ContentValues values = new ContentValues();
		values.put(COLUMN_DATE, data.getTime());
		values.put(COLUMN_WEIGHT, data.getWeight());
		return values;
	}

	public static void store(SQLiteDatabase db, WeightData data) {
		long id = db.insert(TABLE_NAME, null, getContentValues(data));
		data.setId(id);
	}

	public static ArrayList<WeightData> loadX(SQLiteDatabase db, int x,
			int offset) {
		return load(db, null, null, ORDER + " LIMIT " + x + " OFFSET " + offset);
	}

	public static ArrayList<WeightData> load(SQLiteDatabase db) {
		return load(db, null, null, ORDER);
	}

	private static ArrayList<WeightData> load(SQLiteDatabase db,
			String selection, String[] selectionArgs, String order) {
		ArrayList<WeightData> result = new ArrayList<WeightData>();

		Cursor cursor = db.query(TABLE_NAME, ALL_COLUMNS, selection,
				selectionArgs, null, null, order);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			long id = cursor.getLong(0);
			Date date = new Date(cursor.getLong(1));
			double weight = cursor.getDouble(2);

			WeightData d = new WeightData(date, weight);
			d.setId(id);

			result.add(d);

			cursor.moveToNext();
		}
		cursor.close();

		return result;
	}

	public static void update(SQLiteDatabase db, WeightData w) {
		db.update(TABLE_NAME, getContentValues(w), BaseColumns._ID + "=?",
				new String[] { Long.toString(w.getId()) });
	}

	public static void delete(SQLiteDatabase db, WeightData w) {
		db.delete(TABLE_NAME, BaseColumns._ID + "=?",
				new String[] { Long.toString(w.getId()) });
	}

	public static WeightData getLastWeight(SQLiteDatabase db) {
		// TODO LOW: Get the latest in time, it already does?
		ArrayList<WeightData> res = load(db, null, null, ORDER + " LIMIT 1");
		if (res.size() == 0) {
			return new WeightData(null, Const.DEFAULT_WEIGHT);
		}
		return res.get(0);
	}
}
