package train.book.db;

import android.database.sqlite.SQLiteDatabase;

public abstract class DB {

	public void deleteAll(SQLiteDatabase db) {
		db.delete(getTableName(), null, null);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL(getCreateCommand());
	}

	public void onUpdate(SQLiteDatabase db) {
		// TODO LOW: Update instead of deleting
		db.execSQL("DROP TABLE IF EXISTS " + getTableName());
	}

	protected abstract String getCreateCommand();

	protected abstract String getTableName();
}
