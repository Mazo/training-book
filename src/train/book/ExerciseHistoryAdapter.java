package train.book;

import java.util.List;

import train.book.data.ExerciseData;
import train.book.data.WorkoutData;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ExerciseHistoryAdapter extends ArrayAdapter<ExerciseData> {

	public ExerciseHistoryAdapter(Context context) {
		super(context, R.layout.exercise_history_table_row);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = View.inflate(getContext(),
					R.layout.exercise_history_table_row, null);
		}
		ExerciseData d = getItem(position);
		if (d != null) {
			TextView exercise = (TextView) convertView
					.findViewById(R.id.textViewExerciseHistoryTableExercise);
			TextView result = (TextView) convertView
					.findViewById(R.id.textViewExerciseHistoryTableResult);
			exercise.setText(d.getExerciseName());
			String res = d.toString();
			if (res.equals("")) {
				res = "-";
			}
			result.setText(res);

		}
		return convertView;
	}

	public void setData(List<ExerciseData> data) {
		clear();
		for (ExerciseData e : data) {
			add(e);
		}
	}

	public void setData(WorkoutData data) {
		clear();
		for (int i = 0; i < data.size(); i++) {
			add(data.get(i));
		}
	}
}