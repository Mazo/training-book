package train.book;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.widget.EditText;

public class NumberPicker extends NumberDecimalPicker {

	public NumberPicker(Context context, AttributeSet attrs) {
		super(context, attrs);

		EditText editText = (EditText) findViewById(R.id.custNumPickerEditText);
		editText.setInputType(InputType.TYPE_CLASS_NUMBER);
	}

	@Override
	protected double getIncreaseValue() {
		return 1;
	}

	@Override
	protected String getText() {
		return Integer.toString((int) value);
	}

}
