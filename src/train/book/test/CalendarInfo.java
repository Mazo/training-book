package train.book.test;

public class CalendarInfo {
	private long id;
	private String name;
	private String displayName;

	public CalendarInfo(String name, String displayName) {
		this(-1, name, displayName);
	}

	public CalendarInfo(long id, String name, String displayName) {
		this.id = id;
		this.name = name;
		this.displayName = displayName;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDisplayName() {
		return displayName;
	}

	@Override
	public String toString() {
		return "ID: " + id + " name: " + name + " displayName: " + displayName;
	}
}
