package train.book.test;

import java.util.Date;

public class EventInfo {
	private long id;
	private long calendarId;
	private String title;
	private String description;
	private long start;
	private long end;

	public EventInfo(long calendarId, String title, String description,
			long start, long end) {
		this(-1, calendarId, title, description, start, end);
	}

	public EventInfo(long id, long calendarId, String title,
			String description, long start, long end) {
		this.id = id;
		this.calendarId = calendarId;
		this.title = title;
		this.description = description;
		this.start = start;
		this.end = end;
	}

	public long getId() {
		return id;
	}

	public long getCalendarId() {
		return calendarId;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public long getStart() {
		return start;
	}

	public long getEnd() {
		return end;
	}

	@Override
	public String toString() {
		return "ID: " + id + " CAL_ID: " + calendarId + " title: " + title
				+ " description: " + description + " start: " + new Date(start)
				+ " end: " + new Date(end);
	}
}
