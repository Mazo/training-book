package train.book.test;

import android.net.Uri;
import android.os.Build;
import android.provider.BaseColumns;

public class CalendarContract {
	public static final Uri CONTENT_URI = Uri
			.parse(Build.VERSION.SDK_INT >= 8 ? "content://com.android.calendar"
					: "content://calendar");

	public static class Calendars implements BaseColumns {
		public static final Uri CONTENT_URI = Uri.withAppendedPath(
				CalendarContract.CONTENT_URI, "calendars");
		public static final String NAME = "name";
		public static final String CALENDAR_DISPLAY_NAME = "displayName";
		// public static final String ACCOUNT_NAME = "_sync_account";
		// public static final String ACCOUNT_TYPE = "_sync_account_type";
		// public static final String CALENDAR_COLOR = "color";
		// public static final String CALENDAR_ACCESS_LEVEL = "access_level";
		// public static final int CAL_ACCESS_OWNER = 700;
		// public static final String SYNC_EVENTS = "sync_events";
		// public static final String OWNER_ACCOUNT = "ownerAccount";

	}

	public static class Events implements BaseColumns {
		public static final Uri CONTENT_URI = Uri.withAppendedPath(
				CalendarContract.CONTENT_URI, "events");
		public static final String CALENDAR_ID = "calendar_id";
		public static final String DESCRIPTION = "description";
		public static final String DTEND = "dtend";
		public static final String DTSTART = "dtstart";
		public static final String TITLE = "title";
		public static final String EVENT_TIMEZONE = "eventTimezone";
	}
}
