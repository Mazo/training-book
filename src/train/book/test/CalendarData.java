package train.book.test;

import java.util.ArrayList;

import train.book.Const;
import train.book.test.CalendarContract.Calendars;
import train.book.test.CalendarContract.Events;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class CalendarData {
	private static final String CALENDAR_NAME = "Training Book History";
	private ContentResolver contentResolver;

	public CalendarData(Context context) {
		contentResolver = context.getContentResolver();
	}

	@SuppressWarnings("unused")
	private ArrayList<EventInfo> loadEvents(long calendarId) {
		ArrayList<EventInfo> result = new ArrayList<EventInfo>();

		Cursor cursor = contentResolver.query(Events.CONTENT_URI, new String[] {
				Events._ID, Events.TITLE, Events.DESCRIPTION, Events.DTSTART,
				Events.DTEND }, Events.CALENDAR_ID + "=" + calendarId, null,
				null);

		while (cursor.moveToNext()) {
			long id = cursor.getLong(0);
			String title = cursor.getString(1);
			String description = cursor.getString(2);
			long start = cursor.getLong(3);
			long end = cursor.getLong(4);

			result.add(new EventInfo(id, calendarId, title, description, start,
					end));
		}
		cursor.close();
		return result;
	}

	private ContentValues getContentValues(EventInfo e) {
		ContentValues values = new ContentValues();
		values.put(Events.CALENDAR_ID, e.getCalendarId());
		values.put(Events.DTSTART, e.getStart());
		values.put(Events.DTEND, e.getEnd());
		values.put(Events.TITLE, e.getTitle());
		values.put(Events.DESCRIPTION, e.getDescription());
		values.put(Events.EVENT_TIMEZONE, Const.DEFAULT_TIMEZONE);
		return values;
	}

	@SuppressWarnings("unused")
	private void updateEvent(EventInfo e) {
		contentResolver.update(Events.CONTENT_URI, getContentValues(e),
				Events._ID + "=" + e.getId(), null);
	}

	@SuppressWarnings("unused")
	private void deleteEvent(EventInfo e) {
		contentResolver.delete(Events.CONTENT_URI,
				Events._ID + "=" + e.getId(), null);
	}

	@SuppressWarnings("unused")
	private void storeEvent(EventInfo e) {
		contentResolver.insert(Events.CONTENT_URI, getContentValues(e));
	}

	private long getTrainingBookCalendarId() {
		Cursor cursor = contentResolver.query(Calendars.CONTENT_URI,
				new String[] { Calendars._ID }, Calendars.NAME + "= ?",
				new String[] { CALENDAR_NAME }, null);

		long id = -1;
		if (cursor.moveToFirst()) {
			id = cursor.getLong(0);
		} else {
			// storeDefaultCalendar();
		}
		cursor.close();
		return id;
	}

	@SuppressWarnings("unused")
	private void storeDefaultCalendar() {
		// TODO LOW: Make the creation of the calendar work or notify the user?
		// ContentValues values = new ContentValues();

		// String account = "something@gmail.com";
		// String accountType = "com.google";

		// values.put(Calendars.ACCOUNT_NAME, account);
		// values.put(Calendars.ACCOUNT_TYPE, accountType);
		// values.put(Calendars.NAME, TEST_NAME);
		// values.put(Calendars.CALENDAR_DISPLAY_NAME, TEST_NAME);
		// values.put(Calendars.CALENDAR_COLOR, Color.BLUE); // Blue
		// values.put(Calendars.CALENDAR_ACCESS_LEVEL,
		// Calendars.CAL_ACCESS_OWNER);
		// values.put(Calendars.OWNER_ACCOUNT, account);
		// values.put(Calendars.SYNC_EVENTS, 1);

		// contentResolver.insert(uri, values);
	}

	public void readCalendar() {
		long calendarId = getTrainingBookCalendarId();
		System.out.println(calendarId + "");

	}
}
