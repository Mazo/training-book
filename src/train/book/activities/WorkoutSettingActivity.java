package train.book.activities;

import java.util.ArrayList;

import train.book.R;
import train.book.data.Exercise;
import train.book.data.Workout;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class WorkoutSettingActivity extends CustomWindow {
	private ArrayAdapter<Exercise> exercises;
	private EditText textName;
	private Workout currentData;
	private static ArrayList<Exercise> editData = new ArrayList<Exercise>();
	private static final int SELECT_EXERCISES = 54;

	private static final int ID_UP = 1;
	private static final int ID_DOWN = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.workout_setting);

		currentData = TrainbookActivity.getEditData();

		ListView lv = (ListView) findViewById(R.id.listViewWorkoutSettingList);
		exercises = new ArrayAdapter<Exercise>(this, R.layout.medium_list_item);
		lv.setAdapter(exercises);

		// TODO LOW: Create better solution for the adapter + array

		// Add long click thing for the list view
		registerForContextMenu(lv);

		lv.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				parent.showContextMenuForChild(view);
			}
		});

		textName = (EditText) findViewById(R.id.editTextWorkoutSettingName);
		textName.setText(currentData.getName());

		updateExerciseList();
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		if (v.getId() == R.id.listViewWorkoutSettingList) {
			menu.setHeaderTitle(getString(R.string.workout_setting_move_exercise));
			menu.add(Menu.NONE, ID_UP, Menu.NONE,
					R.string.workout_setting_move_up);
			menu.add(Menu.NONE, ID_DOWN, Menu.NONE,
					R.string.workout_setting_move_down);
		} else {
			super.onCreateContextMenu(menu, v, menuInfo);
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case ID_UP: {
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
					.getMenuInfo();
			Exercise d = exercises.getItem(info.position);
			currentData.moveExercise(d, -1);
			updateExerciseList();
			return true;
		}
		case ID_DOWN: {
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
					.getMenuInfo();
			Exercise d = exercises.getItem(info.position);
			currentData.moveExercise(d, 1);
			updateExerciseList();
			return true;
		}
		default:
			return super.onContextItemSelected(item);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == SELECT_EXERCISES) {
			if (resultCode == RESULT_OK) {
				currentData.clearExercises();
				for (Exercise e : editData) {
					currentData.addExercise(e);
				}
				updateExerciseList();
			}
		}
	}

	public void onClickSelect(View v) {
		editData.clear();
		for (int i = 0; i < currentData.getNumberOfExercises(); i++) {
			editData.add(currentData.getExercise(i));
		}
		Intent intent = new Intent(WorkoutSettingActivity.this,
				ExerciseSelectActivity.class);
		startActivityForResult(intent, SELECT_EXERCISES);
	}

	public void onClickSave(View v) {
		updateDataFromFields();
		setResult(RESULT_OK);
		finish();
	}

	public void onClickCancel(View v) {
		setResult(RESULT_CANCELED);
		finish();
	}

	private void updateDataFromFields() {
		String name = textName.getText().toString();
		name.trim();
		// Make sure name isn't empty
		name = name.replaceAll("\n", "");
		if (name.replaceAll(" ", "").length() == 0) {
			name = getString(R.string.no_name);
		}
		currentData.setName(name);
	}

	private void updateExerciseList() {
		exercises.clear();
		for (int i = 0; i < currentData.getNumberOfExercises(); i++) {
			exercises.add(currentData.getExercise(i));
		}
	}

	public static ArrayList<Exercise> getEditData() {
		return editData;
	}
}
