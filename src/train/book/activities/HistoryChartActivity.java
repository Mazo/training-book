package train.book.activities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeSet;

import train.book.Const;
import train.book.R;
import train.book.chart.LineChart;
import train.book.data.Exercise;
import train.book.data.ExerciseData;
import train.book.data.HistoryData;
import train.book.data.WeightData;
import train.book.data.Workout;
import train.book.data.WorkoutData;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Bundle;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ListView;
import android.widget.ProgressBar;

public class HistoryChartActivity extends CustomWindow implements
		OnTouchListener {
	private static ProgressBar progressTitle = null;
	private LineChart lineChart;
	private float downX;
	private static final int MIN_DISTANCE = 100;
	private ArrayList<Exercise> exercises;
	private ArrayList<HistoryData> historyList;

	private static final int DIALOG_PICK_CHART = 52;

	private int index = 0;
	private static final String CHART_INDEX = "hc_index";

	private AlertDialog selectChartDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.history_chart);

		progressTitle = (ProgressBar) findViewById(R.id.progressBarTitle);

		// Create a shallow copy
		ArrayList<Exercise> tmp = TrainbookActivity.getExerciseList();
		exercises = new ArrayList<Exercise>(tmp);

		// Create a set with all active exercises
		ArrayList<Workout> workouts = TrainbookActivity.getWorkoutList();
		final TreeSet<Long> activeExercises = new TreeSet<Long>();
		for (Workout w : workouts) {
			if (w.doShow()) {
				for (int i = 0; i < w.getNumberOfExercises(); i++) {
					activeExercises.add(w.getExercise(i).getId());
				}
			}
		}

		// Sort the exercises, show active first!
		Collections.sort(exercises, new Comparator<Exercise>() {
			public int compare(Exercise lhs, Exercise rhs) {
				boolean a = activeExercises.contains(lhs.getId());
				boolean b = activeExercises.contains(rhs.getId());
				if (a == b) {
					return lhs.getName().compareTo(rhs.getName());
				} else {
					return a ? -1 : 1;
				}
			}
		});

		historyList = HistoryActivity.getHistoryList();

		lineChart = (LineChart) findViewById(R.id.lineChartHistoryChartChart);
		lineChart.setOnTouchListener(this);

		// Build selector list
		CharSequence[] items = new CharSequence[exercises.size() + 1];
		items[0] = getString(R.string.self_weight);
		for (int i = 0; i < exercises.size(); i++) {
			items[i + 1] = exercises.get(i).getName();
		}

		selectChartDialog = new AlertDialog.Builder(this)
				.setTitle(getString(R.string.history_chart_menu_select_chart))
				.setSingleChoiceItems(items, index,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int item) {
								dismissDialog(DIALOG_PICK_CHART);
								index = item;
								updateChart();
							}
						}).create();
	}

	@Override
	protected void onPause() {
		if (isFinishing()) {
			// Clean up static objects...
			progressTitle = null;
		}
		super.onPause();
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		switch (id) {
		case DIALOG_PICK_CHART:
			ListView lv = selectChartDialog.getListView();
			lv.clearChoices();
			lv.setItemChecked(index, true);
			break;
		default:
			super.onPrepareDialog(id, dialog);
			break;
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_PICK_CHART:
			return selectChartDialog;
		default:
			return super.onCreateDialog(id);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.history_chart_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.history_chart_menu_select_chart:
			showDialog(DIALOG_PICK_CHART);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onResume() {
		// Show the progress bar
		progressTitle.setVisibility(HistoryActivity.isUpdating() ? View.VISIBLE
				: View.GONE);

		updateChart();
		super.onResume();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt(CHART_INDEX, index);
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		index = savedInstanceState.getInt(CHART_INDEX);
		super.onRestoreInstanceState(savedInstanceState);
	}

	private void updateChart() {
		// TODO LOW: Make multiple lines better?
		ArrayList<Pair<Integer, ArrayList<PointF>>> dummyData = getExerciseHistory();
		lineChart.setFormat(Const.chart_axis_format, null);
		if (index == 0) {
			lineChart.setTitle(getString(R.string.self_weight));
		} else {
			lineChart.setTitle(exercises.get(index - 1).getName());
		}
		lineChart.clearData();
		for (Pair<Integer, ArrayList<PointF>> p : dummyData) {
			lineChart.addData(p.first, p.second);
		}
	}

	private ArrayList<Pair<Integer, ArrayList<PointF>>> getExerciseHistory() {
		ArrayList<Pair<Integer, ArrayList<PointF>>> res = new ArrayList<Pair<Integer, ArrayList<PointF>>>();

		if (index == 0) {
			ArrayList<PointF> tmp = new ArrayList<PointF>();
			for (HistoryData d : historyList) {
				if (d instanceof WeightData) {
					tmp.add(new PointF(d.getTime(), (float) ((WeightData) d)
							.getWeight()));
				}
			}
			res.add(new Pair<Integer, ArrayList<PointF>>(Color.BLUE, tmp));
		} else {
			ArrayList<PointF> d1 = new ArrayList<PointF>();
			ArrayList<PointF> d2 = new ArrayList<PointF>();
			long exerciseId = exercises.get(index - 1).getId();
			for (HistoryData h : historyList) {
				if (h instanceof WorkoutData) {
					WorkoutData d = (WorkoutData) h;
					for (int i = 0; i < d.size(); i++) {
						ExerciseData data = d.get(i);
						if (data.size() == 0) {
							break;
						}
						if (data.getExerciseId() == exerciseId) {
							switch (data.getType()) {
							case WEIGHT_REPS:
							case TIME:
								d1.add(new PointF(d.getTime(), (float) data
										.getD1Max()));
								d2.add(new PointF(d.getTime(), (float) data
										.getD2Max()));
								break;
							case REPS:
							case DISTANCE_TIME:
								d1.add(new PointF(d.getTime(), data.getD1Max()));
								break;
							case UNKNOWN:
								break;
							}
						}
					}
				}
			}
			res.add(new Pair<Integer, ArrayList<PointF>>(Color.RED, d1));
			res.add(new Pair<Integer, ArrayList<PointF>>(Color.BLUE, d2));
		}
		return res;
	}

	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			downX = event.getX();
			return true;
		case MotionEvent.ACTION_UP:
			float deltaX = downX - event.getX();
			if (Math.abs(deltaX) >= MIN_DISTANCE) {
				if (deltaX < 0) {
					onSwipeRight();
				} else {
					onSwipLeft();
				}
			}
			return true;
		default:
			return super.onTouchEvent(event);
		}
	}

	private void onSwipLeft() {
		viewNext();
	}

	private void onSwipeRight() {
		viewPrevious();
	}

	private void viewNext() {
		index++;
		if (index > exercises.size()) {
			index = 0; // wrap
		}
		updateChart();
	}

	private void viewPrevious() {
		index--;
		if (index < 0) {
			index = exercises.size(); // wrap
		}
		updateChart();
	}

	public static void hideProgressBar() {
		if (progressTitle != null) {
			progressTitle.setVisibility(View.GONE);
		}
	}
}
