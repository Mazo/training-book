package train.book.activities;

import train.book.R;
import train.book.data.Exercise;
import train.book.data.ExerciseType;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class ExerciseSettingActivity extends CustomWindow {
	private RadioButton radioWeRe, radioRe, radioDiTi, radioTi;
	private EditText textName;
	private Exercise currentData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.exercise_setting);

		String callerClass = getCallingActivity().getClassName();
		if (callerClass.compareTo(ExerciseSelectActivity.class.getName()) == 0) {
			currentData = ExerciseSelectActivity.getEditData();
		} else if (callerClass.compareTo(ExercisesActivity.class.getName()) == 0) {
			currentData = ExercisesActivity.getEditData();
		} else {
			setResult(RESULT_CANCELED);
			finish();
			return;
		}

		textName = (EditText) findViewById(R.id.editTextExerciseSetting);
		radioWeRe = (RadioButton) findViewById(R.id.radioButtonWeRe);
		radioRe = (RadioButton) findViewById(R.id.radioButtonRe);
		radioDiTi = (RadioButton) findViewById(R.id.radioButtonDiTi);
		radioTi = (RadioButton) findViewById(R.id.radioButtonTi);

		textName.setText(currentData.getName());
		switch (currentData.getType()) {
		case WEIGHT_REPS:
			radioWeRe.setChecked(true);
			break;
		case REPS:
			radioRe.setChecked(true);
			break;
		case DISTANCE_TIME:
			radioDiTi.setChecked(true);
			break;
		case TIME:
			radioTi.setChecked(true);
			break;
		default:
			radioWeRe.setChecked(true); // Default selection
			break;
		}
	}

	private void updateDataFromFields() {
		String name = textName.getText().toString();
		name.trim();
		// Make sure name isn't empty
		name = name.replaceAll("\n", "");
		if (name.replaceAll(" ", "").length() == 0) {
			name = getString(R.string.no_name);
		}
		ExerciseType type;

		// Check which box is checked
		if (radioWeRe.isChecked()) {
			type = ExerciseType.WEIGHT_REPS;
		} else if (radioRe.isChecked()) {
			type = ExerciseType.REPS;
		} else if (radioDiTi.isChecked()) {
			type = ExerciseType.DISTANCE_TIME;
		} else {
			// Assume the last one is checked...
			type = ExerciseType.TIME;
		}

		currentData.setName(name);
		currentData.setType(type);
	}

	public void onClickSave(View v) {
		updateDataFromFields();
		setResult(RESULT_OK);
		finish();
	}

	public void onClickCancel(View v) {
		setResult(RESULT_CANCELED);
		finish();
	}
}
