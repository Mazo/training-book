package train.book.activities;

import java.util.ArrayList;
import java.util.Collections;

import train.book.ExerciseSelectAdapter;
import train.book.R;
import train.book.data.Exercise;
import train.book.db.DBExercise;
import train.book.db.DataBaseHandler;
import train.book.db.DataBaseHandler.DBRunnable;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class ExerciseSelectActivity extends CustomWindow {
	private ExerciseSelectAdapter exerciseAdapter;
	private ArrayList<Exercise> exerciseList = new ArrayList<Exercise>();
	private ArrayList<Exercise> currentData;
	private static Exercise editData;

	private static final int NEW_EXERCISE = 141;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.exercise_select);

		DataBaseHandler.init(getApplicationContext());

		exerciseList = TrainbookActivity.getExerciseList();

		currentData = WorkoutSettingActivity.getEditData();

		ListView lv = (ListView) findViewById(R.id.listViewExerciseSelect);
		exerciseAdapter = new ExerciseSelectAdapter(this);
		lv.setAdapter(exerciseAdapter);

		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				@SuppressWarnings("unchecked")
				Exercise e = ((Pair<Exercise, Boolean>) parent
						.getItemAtPosition(position)).first;
				final long exerciseId = e.getId();
				CheckBox c = (CheckBox) view
						.findViewById(R.id.checkBoxListItem);
				boolean checked = !c.isChecked();
				c.setChecked(checked);
				if (checked) {
					currentData.add(e);

				} else {
					for (int i = 0; i < currentData.size(); i++) {
						if (currentData.get(i).getId() == exerciseId) {
							currentData.remove(i);
							break;
						}
					}
				}

			}
		});

		updateList();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == NEW_EXERCISE) {
			if (resultCode == RESULT_OK) {
				exerciseList.add(editData);
				Collections.sort(exerciseList);
				updateList();
				final Exercise tmp = editData;
				DataBaseHandler.post(new DBRunnable() {

					@Override
					public void run(SQLiteDatabase db) {
						DBExercise.store(db, tmp);
					}
				});
			}
		}
	}

	public void onClickOK(View v) {
		setResult(RESULT_OK);
		finish();
	}

	public void onClickCancel(View v) {
		setResult(RESULT_CANCELED);
		finish();
	}

	public void onClickCreate(View v) {
		editData = new Exercise("");
		Intent intent = new Intent(ExerciseSelectActivity.this,
				ExerciseSettingActivity.class);
		startActivityForResult(intent, NEW_EXERCISE);
	}

	private void updateList() {
		exerciseAdapter.clear();
		for (Exercise e : exerciseList) {
			exerciseAdapter.add(new Pair<Exercise, Boolean>(e, isSelected(e)));
		}
	}

	private boolean isSelected(Exercise e) {
		// TODO LOW: Make the searching more efficient?
		for (Exercise i : currentData) {
			if (i.getId() == e.getId()) {
				return true;
			}
		}
		return false;
	}

	public static Exercise getEditData() {
		return editData;
	}
}
