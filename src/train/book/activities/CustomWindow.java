package train.book.activities;

import train.book.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.Window;

public class CustomWindow extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);

		setContentView(R.layout.empty_view);

		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
				R.layout.window_title);
	}
}
