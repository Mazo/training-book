package train.book.activities;

import java.io.File;
import java.util.ArrayList;

import train.book.R;
import train.book.db.Backup;
import train.book.db.DataBaseHandler;
import train.book.db.DataBaseHandler.DBRunnable;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.view.Window;

public class SettingsActivity extends PreferenceActivity {
	private boolean backupCancel = false;
	private ProgressDialog backupSaveDialog;

	private static final int DIALOG_BACKUP_PROGRESS = 3;
	private static final int SEND_BACKUP = 44;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// Custom title
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.preferences);

		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
				R.layout.window_title);

		Preference customWorkouts = findPreference(getString(R.string.preferences_custom_workouts));
		Preference customExercises = findPreference(getString(R.string.preferences_custom_exercises));
		Preference customBackup = findPreference(getString(R.string.preferences_custom_backup));
		Preference customRestore = findPreference(getString(R.string.preferences_custom_restore));

		customWorkouts
				.setOnPreferenceClickListener(new OnPreferenceClickListener() {

					public boolean onPreferenceClick(Preference preference) {
						showManageWorkouts();
						return true;
					}
				});

		customExercises
				.setOnPreferenceClickListener(new OnPreferenceClickListener() {

					public boolean onPreferenceClick(Preference preference) {
						showExercises();
						return true;
					}
				});

		customBackup
				.setOnPreferenceClickListener(new OnPreferenceClickListener() {

					public boolean onPreferenceClick(Preference preference) {
						sendBackup();
						return true;
					}
				});

		customRestore
				.setOnPreferenceClickListener(new OnPreferenceClickListener() {

					public boolean onPreferenceClick(Preference preference) {
						// TODO LOW: Create import
						return true;
					}
				});

		backupSaveDialog = new ProgressDialog(this);
		backupSaveDialog.setCancelable(true);
		backupSaveDialog.setOnCancelListener(new OnCancelListener() {

			public void onCancel(DialogInterface dialog) {
				backupCancel = true;
			}
		});
		backupSaveDialog
				.setMessage(getString(R.string.backup_creating_message));
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_BACKUP_PROGRESS:
			return backupSaveDialog;
		}
		return super.onCreateDialog(id);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == SEND_BACKUP) {
			/*
			 * DataBaseHandler.post(new DBRunnable() {
			 * 
			 * @Override public void run(SQLiteDatabase db) { // Delete backup
			 * file. Backup.delete(SettingsActivity.this); } });
			 */
		}
	}

	private void showManageWorkouts() {
		Intent intent = new Intent(SettingsActivity.this,
				ManageWorkoutsActivity.class);
		startActivity(intent);
	}

	private void showExercises() {
		Intent intent = new Intent(SettingsActivity.this,
				ExercisesActivity.class);
		startActivity(intent);
	}

	private void sendBackup() {
		backupCancel = false;
		showDialog(DIALOG_BACKUP_PROGRESS);
		DataBaseHandler.post(new DBRunnable() {

			@Override
			public void run(SQLiteDatabase db) {
				final ArrayList<Uri> uris = new ArrayList<Uri>();
				File file1 = Backup.createData(SettingsActivity.this, db);
				File file2 = Backup.createSlow(SettingsActivity.this, db);

				if (file1 != null) {
					uris.add(Uri.fromFile(file1));
				}
				if (file2 != null) {
					uris.add(Uri.fromFile(file2));
				}

				if (uris.size() == 0) {
					removeDialog(DIALOG_BACKUP_PROGRESS);
				} else {
					runOnUiThread(new Runnable() {

						public void run() {
							// TODO LOW: Fix the dialog, not hiding when
							// rotated.
							removeDialog(DIALOG_BACKUP_PROGRESS);
							if (!backupCancel) {
								Intent i = new Intent(
										Intent.ACTION_SEND_MULTIPLE);
								i.setType("text/plain");
								i.putExtra(Intent.EXTRA_SUBJECT,
										getString(R.string.backup_subject));
								i.putExtra(Intent.EXTRA_TEXT,
										getString(R.string.backup_message));
								i.putExtra(Intent.EXTRA_STREAM, uris);

								startActivityForResult(
										Intent.createChooser(
												i,
												getString(R.string.backup_title_send_to)),
										SEND_BACKUP);
							}
						}
					});
				}
			}
		});
	}
}
