package train.book.activities.edit;

import train.book.R;
import android.os.Bundle;
import android.view.View;

public class WorkoutEditTimeActivity extends WorkoutEditBaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected View getView() {
		return getLayoutInflater().inflate(R.layout.workout_step_base_time,
				null);
	}

	@Override
	protected int getChangeSetLayout() {
		return R.layout.workout_step_change_time_data;
	}
}
