package train.book.activities.edit;

import train.book.NumberDecimalPicker;
import train.book.NumberPicker;
import train.book.R;
import train.book.activities.CustomWindow;
import train.book.activities.ExerciseHistoryActivity;
import train.book.data.EditExerciseData;
import train.book.data.ExerciseSet;
import train.book.data.ExerciseType;
import train.book.db.DataBaseHandler;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;

public abstract class WorkoutEditBaseActivity extends CustomWindow {
	private ArrayAdapter<ExerciseSet> exerciseSetAdapter;

	private static final int ID_EDIT = 11;
	private static final int ID_DELETE = 12;

	private static final String EDIT_POSITION = "EdPos";
	private static final String TMP_D1_VALUE = "T_D1_Val";
	private static final String TMP_D2_VALUE = "T_D2_Val";

	private AlertDialog editSetDialog;

	private static final int DIALOG_EDIT_SET = 1;

	private long d1Value = 0;
	private double d2Value = 0;
	private int editPosition = -1;
	private NumberPicker dialogD1;
	private NumberDecimalPicker dialogD2;
	private EditExerciseData currentData;

	// protected to provide change handlers and such
	protected NumberPicker pickerD1;
	protected NumberDecimalPicker pickerD2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.workout_edit_base);

		DataBaseHandler.init(getApplicationContext());

		LinearLayout container = (LinearLayout) findViewById(R.id.linearLayoutWorkoutStepMiddle);
		View v = getView();
		container.addView(v, 0);

		currentData = ExerciseHistoryActivity.getEditData();

		pickerD1 = (NumberPicker) v.findViewById(R.id.numberPickerStepD1);
		pickerD2 = (NumberDecimalPicker) v
				.findViewById(R.id.numberDecimalPickerStepD2);

		// Update the title and so on...
		TextView title = (TextView) findViewById(R.id.textViewWorkoutStepTitle);
		title.setText(currentData.getExerciseName());
		exerciseSetAdapter = new ArrayAdapter<ExerciseSet>(this,
				R.layout.workout_step_table_row);
		for (int i = 0; i < currentData.size(); i++) {
			exerciseSetAdapter.add(currentData.get(i));
		}

		ListView lv = (ListView) findViewById(R.id.listViewWorkoutStepList);
		lv.setAdapter(exerciseSetAdapter);

		// Add long click thing for the list view
		registerForContextMenu(lv);

		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				startEditSet((ExerciseSet) parent.getItemAtPosition(position));
			}
		});

		View layout = getLayoutInflater().inflate(getChangeSetLayout(), null);
		dialogD1 = (NumberPicker) layout
				.findViewById(R.id.numberPickerChangeD1);
		dialogD2 = (NumberDecimalPicker) layout
				.findViewById(R.id.numberDecimalPickerChangeD2);
		editSetDialog = new AlertDialog.Builder(this)
				.setView(layout)
				.setTitle(getString(R.string.dialog_edit_set_title))
				.setPositiveButton(getString(R.string.button_ok),
						new OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								updateSet();
							}
						})
				.setNegativeButton(getString(R.string.button_cancel), null)
				.create();
	}

	protected abstract View getView();

	protected abstract int getChangeSetLayout();

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt(EDIT_POSITION, editPosition);
		outState.putLong(TMP_D1_VALUE, (long) dialogD1.getValue());
		outState.putDouble(TMP_D2_VALUE, dialogD2.getValue());
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		editPosition = savedInstanceState.getInt(EDIT_POSITION);
		d1Value = savedInstanceState.getLong(TMP_D1_VALUE);
		d2Value = savedInstanceState.getDouble(TMP_D2_VALUE);
		super.onRestoreInstanceState(savedInstanceState);
	}

	@Override
	protected void onPause() {
		currentData.setD1Value((long) pickerD1.getValue());
		currentData.setD2Value(pickerD2.getValue());
		super.onPause();
	}

	@Override
	protected void onResume() {
		pickerD1.setValue(currentData.getD1Value());
		pickerD2.setValue(currentData.getD2Value());
		dialogD1.setValue(d1Value);
		dialogD2.setValue(d2Value);

		updateDialogValues();

		super.onResume();
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		if (v.getId() == R.id.listViewWorkoutStepList) {
			menu.setHeaderTitle(R.string.workout_step_manage_set);
			menu.add(Menu.NONE, ID_EDIT, Menu.NONE, R.string.workout_edit);
			menu.add(Menu.NONE, ID_DELETE, Menu.NONE, R.string.workout_delete);
		} else {
			super.onCreateContextMenu(menu, v, menuInfo);
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case ID_EDIT: {
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
					.getMenuInfo();
			ExerciseSet s = exerciseSetAdapter.getItem(info.position);
			startEditSet(s);
			return true;
		}
		case ID_DELETE: {
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
					.getMenuInfo();
			ExerciseSet s = exerciseSetAdapter.getItem(info.position);
			deleteSet(s);
			return true;
		}
		default:
			return super.onContextItemSelected(item);
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_EDIT_SET:
			return editSetDialog;
		}
		return super.onCreateDialog(id);
	}

	public void onClickAddSet(View v) {
		long d1 = (long) pickerD1.getValue();
		double d2 = pickerD2.getValue();

		ExerciseSet s = new ExerciseSet(d1, d2, currentData.getType());
		s.setSetId(currentData.getId());
		exerciseSetAdapter.add(s);

		// Also update results
		currentData.addSet(s);
	}

	private void startEditSet(ExerciseSet s) {
		editPosition = exerciseSetAdapter.getPosition(s);
		d1Value = s.getD1();
		d2Value = s.getD2();
		updateDialogValues();
		showDialog(DIALOG_EDIT_SET);
	}

	private void updateDialogValues() {
		dialogD1.setValue(d1Value);
		dialogD2.setValue(d2Value);
	}

	private void updateSet() {
		dismissDialog(DIALOG_EDIT_SET);
		long d1 = (long) dialogD1.getValue();
		double d2 = dialogD2.getValue();

		// Update in the result...
		ExerciseSet editSet = currentData.get(editPosition);
		editSet.setD1(d1);
		editSet.setD2(d2);
		exerciseSetAdapter.notifyDataSetChanged();
	}

	private void deleteSet(ExerciseSet s) {
		int index = exerciseSetAdapter.getPosition(s);
		exerciseSetAdapter.remove(s);

		// Also update results
		currentData.remove(index);
	}

	public void onClickOK(View v) {
		setResult(RESULT_OK);
		finish();
	}

	public void onClickCancel(View v) {
		setResult(RESULT_CANCELED);
		finish();
	}

	public static Class<?> getActivity(ExerciseType t) {
		switch (t) {
		case WEIGHT_REPS:
			return WorkoutEditWeightRepsActivity.class;
		case REPS:
			return WorkoutEditRepsActivity.class;
		case DISTANCE_TIME:
			return WorkoutEditDistanceTimeActivity.class;
		case TIME:
			return WorkoutEditTimeActivity.class;
		default:
			return WorkoutEditWeightRepsActivity.class;
		}
	}
}
