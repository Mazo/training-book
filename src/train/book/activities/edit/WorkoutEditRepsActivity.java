package train.book.activities.edit;

import train.book.R;
import android.os.Bundle;
import android.view.View;

public class WorkoutEditRepsActivity extends WorkoutEditBaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected View getView() {
		return getLayoutInflater().inflate(R.layout.workout_step_base_reps,
				null);
	}

	@Override
	protected int getChangeSetLayout() {
		return R.layout.workout_step_change_reps_data;
	}
}
