package train.book.activities.edit;

import train.book.Const;
import train.book.R;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

public class WorkoutEditDistanceTimeActivity extends WorkoutEditBaseActivity {
	private TextView speed;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		speed = (TextView) findViewById(R.id.textViewWorkoutStepSpeed);

		TextWatcher w = new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				updateSpeed();
			}

			public void afterTextChanged(Editable s) {
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
		};
		pickerD1.addTextChangedListener(w);
		pickerD2.addTextChangedListener(w);
	}

	private void updateSpeed() {
		double time = pickerD1.getValue() / 60;
		double distance = pickerD2.getValue();

		String res;
		if (time != 0) {
			res = Const.one_decimal_format.format(distance / time);
		} else {
			res = Const.one_decimal_format.format(0.0);
		}
		speed.setText(res + " " + Const.SPEED_UNIT);
	}

	@Override
	protected void onResume() {
		updateSpeed();
		super.onResume();
	}

	@Override
	protected View getView() {
		return getLayoutInflater().inflate(
				R.layout.workout_step_base_distance_time, null);
	}

	@Override
	protected int getChangeSetLayout() {
		return R.layout.workout_step_change_distance_time_data;
	}
}
