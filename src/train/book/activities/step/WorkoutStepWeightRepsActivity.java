package train.book.activities.step;

import train.book.Const;
import train.book.R;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

public class WorkoutStepWeightRepsActivity extends WorkoutStepBaseActivity {
	private TextView compare;
	private int oldColor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		compare = (TextView) findViewById(R.id.textViewWorkoutStepCompare);
		oldColor = compare.getTextColors().getDefaultColor();

		pickerD2.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				updateCompare();
			}

			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}

			public void afterTextChanged(Editable arg0) {
			}
		});
	}

	@Override
	protected void onPreviousUpdate() {
		updateCompare();
	}

	private void updateCompare() {
		if (previous != null) {
			double res = pickerD2.getValue() - previous.getD2Max();
			if (res == 0) {
				compare.setTextColor(oldColor);
			} else if (res < 0) {
				compare.setTextColor(Color.RED);
			} else {
				compare.setTextColor(Color.GREEN);
			}
			compare.setText(Const.one_decimal_format.format(res));
		}
	}

	@Override
	protected View getView() {
		return getLayoutInflater().inflate(
				R.layout.workout_step_base_weight_reps, null);
	}

	@Override
	protected int getChangeSetLayout() {
		return R.layout.workout_step_change_weight_reps_data;
	}
}
