package train.book.activities.step;

import train.book.ExerciseHistoryAdapter;
import train.book.R;
import train.book.activities.CustomWindow;
import train.book.activities.TrainbookActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class WorkoutEndActivity extends CustomWindow {
	private ExerciseHistoryAdapter summaryList;
	private EditText textComment;
	private int stepIndex;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.workout_step_end);

		stepIndex = getIntent().getIntExtra(
				TrainbookActivity.WORKOUT_STEP_INDEX, -1);

		summaryList = new ExerciseHistoryAdapter(this);
		ListView lv = (ListView) findViewById(R.id.listViewWorkoutEndList);
		lv.setAdapter(summaryList);

		textComment = (EditText) findViewById(R.id.editTextWorkoutEndComment);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			onClickPrevious(null);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onResume() {
		clickedBack = false;
		updateHistoryList();
		super.onResume();
	}

	private void updateHistoryList() {
		summaryList.setData(TrainbookActivity.getActiveData());
	}

	private boolean clickedBack = false;
	private Toast t = null;

	public void onClickPrevious(View v) {
		if (stepIndex == 0 && !TrainbookActivity.isLast(stepIndex)
				&& !clickedBack) {
			t = Toast.makeText(this, R.string.press_again_to_cancel,
					Toast.LENGTH_SHORT);
			t.show();
			clickedBack = true;
		} else {
			if (t != null) {
				t.cancel();
			}
			setResult(RESULT_CANCELED);
			finish();
		}
	}

	public void onClickFinish(View v) {
		if (t != null) {
			t.cancel();
		}
		TrainbookActivity.setWorkoutComment(textComment.getText().toString());
		setResult(RESULT_OK);
		finish();
	}
}
