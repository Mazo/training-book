package train.book.activities.step;

import train.book.NumberDecimalPicker;
import train.book.NumberPicker;
import train.book.R;
import train.book.activities.CustomWindow;
import train.book.activities.TrainbookActivity;
import train.book.data.ActiveExerciseData;
import train.book.data.ExerciseData;
import train.book.data.ExerciseSet;
import train.book.data.ExerciseType;
import train.book.db.DBExerciseData;
import train.book.db.DataBaseHandler;
import train.book.db.DataBaseHandler.DBRunnable;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;

public abstract class WorkoutStepBaseActivity extends CustomWindow {
	private ListView listView;
	private ArrayAdapter<ExerciseSet> list;

	private static final int NEXT_WORKOUT = 2;
	private static final int END_WORKOUT = 3;

	private static final int ID_EDIT = 11;
	private static final int ID_DELETE = 12;

	private static final String IS_FIRST = "Is_First";
	private static final String EDIT_POSITION = "EdPos";
	private static final String TMP_D1_VALUE = "T_D1_Val";
	private static final String TMP_D2_VALUE = "T_D2_Val";

	private AlertDialog editSetDialog;

	private static final int DIALOG_EDIT_SET = 1;

	private boolean isFirst = true;
	private long d1Value = 0;
	private double d2Value = 0;
	private int editPosition = -1;
	private NumberPicker dialogD1;
	private NumberDecimalPicker dialogD2;
	private int stepIndex;
	private ActiveExerciseData currentData;

	// protected to provide change handlers and such
	protected NumberPicker pickerD1;
	protected NumberDecimalPicker pickerD2;
	protected ExerciseData previous;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.workout_step_base);

		DataBaseHandler.init(getApplicationContext());

		// TODO LOW: Create exercise selector in menu

		LinearLayout container = (LinearLayout) findViewById(R.id.linearLayoutWorkoutStepMiddle);
		View v = getView();
		container.addView(v, 0);

		stepIndex = getIntent().getIntExtra(
				TrainbookActivity.WORKOUT_STEP_INDEX, -1);
		currentData = TrainbookActivity.getActiveExerciseData(stepIndex);
		previous = currentData.getPrevious();

		pickerD1 = (NumberPicker) v.findViewById(R.id.numberPickerStepD1);
		pickerD2 = (NumberDecimalPicker) v
				.findViewById(R.id.numberDecimalPickerStepD2);

		// Update the title and so on...
		TextView title = (TextView) findViewById(R.id.textViewWorkoutStepTitle);
		title.setText(currentData.getTitle(stepIndex));
		list = new ArrayAdapter<ExerciseSet>(this,
				R.layout.workout_step_table_row);
		for (int i = 0; i < currentData.size(); i++) {
			list.add(currentData.get(i));
		}

		listView = (ListView) findViewById(R.id.listViewWorkoutStepList);
		listView.setAdapter(list);

		// Add long click thing for the list view
		registerForContextMenu(listView);

		listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				startEditSet((ExerciseSet) parent.getItemAtPosition(position));
			}
		});

		View layout = getLayoutInflater().inflate(getChangeSetLayout(), null);
		dialogD1 = (NumberPicker) layout
				.findViewById(R.id.numberPickerChangeD1);
		dialogD2 = (NumberDecimalPicker) layout
				.findViewById(R.id.numberDecimalPickerChangeD2);
		editSetDialog = new AlertDialog.Builder(this)
				.setView(layout)
				.setTitle(getString(R.string.dialog_edit_set_title))
				.setPositiveButton(getString(R.string.button_ok),
						new OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								updateSet();
							}
						})
				.setNegativeButton(getString(R.string.button_cancel), null)
				.create();
	}

	protected abstract View getView();

	protected abstract int getChangeSetLayout();

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putBoolean(IS_FIRST, isFirst);
		outState.putInt(EDIT_POSITION, editPosition);
		outState.putLong(TMP_D1_VALUE, (long) dialogD1.getValue());
		outState.putDouble(TMP_D2_VALUE, dialogD2.getValue());
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		isFirst = savedInstanceState.getBoolean(IS_FIRST);
		editPosition = savedInstanceState.getInt(EDIT_POSITION);
		d1Value = savedInstanceState.getLong(TMP_D1_VALUE);
		d2Value = savedInstanceState.getDouble(TMP_D2_VALUE);
		super.onRestoreInstanceState(savedInstanceState);
	}

	@Override
	protected void onResume() {
		clickedBack = false;

		if (currentData.isFirst()) {
			currentData.setFirst(false);
			updatePrevious();
		}
		pickerD1.setValue(currentData.getD1Value());
		pickerD2.setValue(currentData.getD2Value());
		dialogD1.setValue(d1Value);
		dialogD2.setValue(d2Value);

		updateDialogValues();

		super.onResume();
	}

	@Override
	protected void onPause() {
		currentData.setD1Value((long) pickerD1.getValue());
		currentData.setD2Value(pickerD2.getValue());
		super.onPause();
	}

	private void updatePrevious() {
		final long exerciseId = currentData.getExerciseId();
		DataBaseHandler.post(new DBRunnable() {

			@Override
			public void run(SQLiteDatabase db) {
				final ExerciseData tmp = DBExerciseData.getPrevious(db,
						exerciseId);

				if (tmp != null) {
					runOnUiThread(new Runnable() {

						public void run() {
							currentData.setPrevious(tmp);
							currentData.setD1Value(tmp.getD1Max());
							currentData.setD2Value(tmp.getD2Max());
							pickerD1.setValue(currentData.getD1Value());
							pickerD2.setValue(currentData.getD2Value());
							previous = currentData.getPrevious();
							onPreviousUpdate();
						}
					});
				}
			}
		});

	}

	protected void onPreviousUpdate() {
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			if (requestCode == NEXT_WORKOUT) {
				setResult(RESULT_OK);
				finish();
			} else if (requestCode == END_WORKOUT) {
				setResult(RESULT_OK);
				finish();
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			onClickPrevious(null);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		if (v.getId() == R.id.listViewWorkoutStepList) {
			menu.setHeaderTitle(R.string.workout_step_manage_set);
			menu.add(Menu.NONE, ID_EDIT, Menu.NONE, R.string.workout_edit);
			menu.add(Menu.NONE, ID_DELETE, Menu.NONE, R.string.workout_delete);
		} else {
			super.onCreateContextMenu(menu, v, menuInfo);
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case ID_EDIT: {
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
					.getMenuInfo();
			ExerciseSet s = list.getItem(info.position);
			startEditSet(s);
			return true;
		}
		case ID_DELETE: {
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
					.getMenuInfo();
			ExerciseSet s = list.getItem(info.position);
			deleteSet(s);
			return true;
		}
		default:
			return super.onContextItemSelected(item);
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_EDIT_SET:
			return editSetDialog;
		}
		return super.onCreateDialog(id);
	}

	public void onClickAddSet(View v) {
		long d1 = (long) pickerD1.getValue();
		double d2 = pickerD2.getValue();

		ExerciseSet s = new ExerciseSet(d1, d2, currentData.getType());
		s.setSetId(currentData.getId());
		list.add(s);

		// Also update results
		currentData.addSet(s);
	}

	private void startEditSet(ExerciseSet s) {
		editPosition = list.getPosition(s);
		d1Value = s.getD1();
		d2Value = s.getD2();
		updateDialogValues();
		showDialog(DIALOG_EDIT_SET);
	}

	private void updateDialogValues() {
		dialogD1.setValue(d1Value);
		dialogD2.setValue(d2Value);
	}

	private void updateSet() {
		dismissDialog(DIALOG_EDIT_SET);
		long d1 = (long) dialogD1.getValue();
		double d2 = dialogD2.getValue();

		// Update in the result...
		ExerciseSet editSet = currentData.get(editPosition);
		editSet.setD1(d1);
		editSet.setD2(d2);
		list.notifyDataSetChanged();
	}

	private void deleteSet(ExerciseSet s) {
		int index = list.getPosition(s);
		list.remove(s);

		// Also update results
		currentData.remove(index);
	}

	private boolean clickedBack = false;
	private Toast t = null;

	public void onClickPrevious(View v) {
		if (stepIndex == 0 && !clickedBack) {
			t = Toast.makeText(this, R.string.press_again_to_cancel,
					Toast.LENGTH_SHORT);
			t.show();
			clickedBack = true;
		} else {
			if (t != null) {
				t.cancel();
			}
			setResult(RESULT_CANCELED);
			finish();
		}
	}

	public void onClickNext(View v) {
		if (t != null) {
			t.cancel();
		}
		Intent intent;
		int requestCode;
		if (TrainbookActivity.hasNext(stepIndex)
				&& !TrainbookActivity.isLast(stepIndex)) {
			ActiveExerciseData next = TrainbookActivity
					.getActiveExerciseData(stepIndex + 1);
			ExerciseType t = next.getType();
			intent = new Intent(WorkoutStepBaseActivity.this, getActivity(t));
			requestCode = NEXT_WORKOUT;
		} else {
			intent = new Intent(WorkoutStepBaseActivity.this,
					WorkoutEndActivity.class);
			requestCode = END_WORKOUT;
		}
		intent.putExtra(TrainbookActivity.WORKOUT_STEP_INDEX, stepIndex + 1);
		startActivityForResult(intent, requestCode);
	}

	public static Class<?> getActivity(ExerciseType t) {
		switch (t) {
		case WEIGHT_REPS:
			return WorkoutStepWeightRepsActivity.class;
		case REPS:
			return WorkoutStepRepsActivity.class;
		case DISTANCE_TIME:
			return WorkoutStepDistanceTimeActivity.class;
		case TIME:
			return WorkoutStepTimeActivity.class;
		default:
			return WorkoutStepWeightRepsActivity.class;
		}
	}
}
