package train.book.activities.step;

import train.book.R;
import android.os.Bundle;
import android.view.View;

public class WorkoutStepTimeActivity extends WorkoutStepBaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected View getView() {
		return getLayoutInflater().inflate(R.layout.workout_step_base_time,
				null);
	}

	@Override
	protected int getChangeSetLayout() {
		return R.layout.workout_step_change_time_data;
	}
}
