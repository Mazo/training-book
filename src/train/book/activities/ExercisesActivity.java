package train.book.activities;

import java.util.ArrayList;
import java.util.Collections;

import train.book.R;
import train.book.data.Exercise;
import train.book.db.DBExercise;
import train.book.db.DataBaseHandler;
import train.book.db.DataBaseHandler.DBRunnable;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;

public class ExercisesActivity extends CustomWindow {
	private static final int ID_EDIT = 8;
	private static final int ID_DELETE = 9;

	private static final int NEW_EXERCISE = 241;
	private static final int EDIT_EXERCISE = 242;

	private static Exercise editData;

	private ArrayList<Exercise> exerciseList;
	private ArrayAdapter<Exercise> exerciseAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.exercises);

		DataBaseHandler.init(getApplicationContext());

		exerciseList = TrainbookActivity.getExerciseList();

		ListView lv = (ListView) findViewById(R.id.listViewExercises);
		exerciseAdapter = new ArrayAdapter<Exercise>(this,
				R.layout.large_list_item, exerciseList);
		lv.setAdapter(exerciseAdapter);

		// Add long click thing for the list view
		registerForContextMenu(lv);

		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				editWorkout((Exercise) parent.getItemAtPosition(position));
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == NEW_EXERCISE) {
			if (resultCode == RESULT_OK) {
				exerciseAdapter.add(editData);
				Collections.sort(exerciseList);
				exerciseAdapter.notifyDataSetChanged();
				final Exercise tmp = editData;
				DataBaseHandler.post(new DBRunnable() {

					@Override
					public void run(SQLiteDatabase db) {
						DBExercise.store(db, tmp);
					}
				});
			}
		} else if (requestCode == EDIT_EXERCISE) {
			if (resultCode == RESULT_OK) {
				editData.commit();
				Collections.sort(exerciseList);
				exerciseAdapter.notifyDataSetChanged();
				final Exercise tmp = editData;
				DataBaseHandler.post(new DBRunnable() {

					@Override
					public void run(SQLiteDatabase db) {
						DBExercise.update(db, tmp);
					}
				});
			} else {
				editData.revert();
			}
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		if (v.getId() == R.id.listViewExercises) {
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
			Exercise e = exerciseAdapter.getItem(info.position);
			menu.setHeaderTitle(e.getName());
			menu.add(Menu.NONE, ID_EDIT, Menu.NONE, R.string.exercise_edit);
			menu.add(Menu.NONE, ID_DELETE, Menu.NONE, R.string.exercise_delete);
		} else {
			super.onCreateContextMenu(menu, v, menuInfo);
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case ID_EDIT: {
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
					.getMenuInfo();
			Exercise e = exerciseAdapter.getItem(info.position);
			editWorkout(e);
			return true;
		}
		case ID_DELETE: {
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
					.getMenuInfo();
			final Exercise e = exerciseAdapter.getItem(info.position);
			exerciseAdapter.remove(e);
			DataBaseHandler.post(new DBRunnable() {

				@Override
				public void run(SQLiteDatabase db) {
					DBExercise.delete(db, e);
				}
			});
			return true;
		}
		default:
			return super.onContextItemSelected(item);
		}
	}

	public void onClickCreate(View v) {
		editData = new Exercise("");
		Intent intent = new Intent(ExercisesActivity.this,
				ExerciseSettingActivity.class);
		startActivityForResult(intent, NEW_EXERCISE);
	}

	private void editWorkout(Exercise e) {
		editData = e;
		editData.startEditing();
		Intent intent = new Intent(ExercisesActivity.this,
				ExerciseSettingActivity.class);
		startActivityForResult(intent, EDIT_EXERCISE);
	}

	public static Exercise getEditData() {
		return editData;
	}
}
