package train.book.activities;

import java.util.ArrayList;

import train.book.WorkoutSelectAdapter;
import train.book.R;
import train.book.data.Workout;
import train.book.db.DBWorkout;
import train.book.db.DataBaseHandler;
import train.book.db.DataBaseHandler.DBRunnable;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class ManageWorkoutsActivity extends CustomWindow {
	private WorkoutSelectAdapter workoutAdapter;
	private ArrayList<Workout> workoutList = new ArrayList<Workout>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.manage_workouts);

		// TODO MED: Add support to edit/delete in here

		DataBaseHandler.init(getApplicationContext());

		workoutList = TrainbookActivity.getWorkoutList();

		ListView lv = (ListView) findViewById(R.id.listViewManageWorkouts);
		workoutAdapter = new WorkoutSelectAdapter(this, workoutList);
		lv.setAdapter(workoutAdapter);

		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				final Workout w = (Workout) parent.getItemAtPosition(position);
				CheckBox c = (CheckBox) view
						.findViewById(R.id.checkBoxListItem);
				boolean checked = !c.isChecked();
				w.setShow(checked);
				c.setChecked(checked);

				TrainbookActivity.setWorkoutShowStateChanged();

				DataBaseHandler.post(new DBRunnable() {

					@Override
					public void run(SQLiteDatabase db) {
						DBWorkout.update(db, w);
					}
				});
			}
		});
	}

	public void onClickCreate(View v) {
		// TODO MED: Create workout button
	}
}
