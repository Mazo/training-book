package train.book.activities;

import java.util.ArrayList;

import train.book.ExerciseHistoryAdapter;
import train.book.R;
import train.book.data.ExerciseData;
import train.book.data.Workout;
import train.book.db.DBExerciseData;
import train.book.db.DataBaseHandler;
import train.book.db.DataBaseHandler.DBRunnable;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

public class PreviousResultActivity extends CustomWindow {
	private Workout currentData;
	private ExerciseHistoryAdapter exerciseList;

	private ProgressBar progressTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.previous_result);

		DataBaseHandler.init(getApplicationContext());

		currentData = TrainbookActivity.getPreviousWorkout();

		progressTitle = (ProgressBar) findViewById(R.id.progressBarTitle);

		exerciseList = new ExerciseHistoryAdapter(this);
		ListView lv = (ListView) findViewById(R.id.listViewPreviousResultList);
		lv.setAdapter(exerciseList);
	}

	@Override
	protected void onResume() {
		// TODO LOW: Maybe not reload every time?
		progressTitle.setVisibility(View.VISIBLE);
		updatePrevious();
		super.onResume();
	}

	private void updatePrevious() {
		DataBaseHandler.post(new DBRunnable() {

			@Override
			public void run(SQLiteDatabase db) {
				final ArrayList<ExerciseData> res = DBExerciseData.getPrevious(
						db, currentData);

				runOnUiThread(new Runnable() {

					public void run() {
						exerciseList.setData(res);
						progressTitle.setVisibility(View.GONE);
					}
				});
			}
		});
	}
}
