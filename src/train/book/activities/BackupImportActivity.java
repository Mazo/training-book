package train.book.activities;

import java.io.File;

import train.book.R;
import train.book.db.Backup;
import train.book.db.DataBaseHandler;
import train.book.db.DataBaseHandler.DBRunnable;
import train.book.db.WorkoutDataOpenHelper;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

public class BackupImportActivity extends CustomWindow {
	private boolean isFirst = true;
	private static final String TMP_IS_FIRST = "tmp_i_f";
	private AlertDialog confirmDialog;
	private ProgressBar progressTitle;
	private TextView importMessage;

	private static final int DIALOG_CONFIRM = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.backup_import);

		// TODO LOW: Create messages?
		// TODO LOW: Maybe file selector?
		progressTitle = (ProgressBar) findViewById(R.id.progressBarTitle);
		importMessage = (TextView) findViewById(R.id.textViewBackupImportMessage);

		confirmDialog = new AlertDialog.Builder(this)
				.setMessage(R.string.backup_import_confirm_message)
				.setPositiveButton(R.string.button_yes, new OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						dismissDialog(DIALOG_CONFIRM);
						importData();
					}
				}).setNegativeButton(R.string.button_no, new OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						dismissDialog(DIALOG_CONFIRM);
						finish();
					}
				}).create();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_CONFIRM:
			return confirmDialog;
		default:
			return super.onCreateDialog(id);
		}

	}

	@Override
	protected void onResume() {
		if (isFirst) {
			isFirst = false;
			showDialog(DIALOG_CONFIRM);
		}
		super.onResume();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putBoolean(TMP_IS_FIRST, isFirst);
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		isFirst = savedInstanceState.getBoolean(TMP_IS_FIRST);
		super.onRestoreInstanceState(savedInstanceState);
	}

	private void importData() {
		Uri data = getIntent().getData();
		if (data != null) {
			final String filePath = data.getEncodedPath();
			if (filePath != null) {
				progressTitle.setVisibility(View.VISIBLE);
				importMessage.setVisibility(View.VISIBLE);
				final File f = new File(filePath);
				DataBaseHandler.init(getApplicationContext());
				DataBaseHandler.post(new DBRunnable() {

					@Override
					public void run(SQLiteDatabase db) {
						WorkoutDataOpenHelper.deleteAll(db);
						if (filePath.endsWith(Backup.RAW)) {
							Backup.importSlow(f, db);
						} else {
							Backup.importData(f, db);
						}

						runOnUiThread(new Runnable() {

							public void run() {
								DataBaseHandler.quit();
								finish();
								// Start main activity
								Intent intent = new Intent(
										BackupImportActivity.this,
										TrainbookActivity.class);
								startActivity(intent);
							}
						});
					}
				});
			}
		}
	}
}
