package train.book.activities;

import java.util.Date;

import train.book.Const;
import train.book.ExerciseHistoryAdapter;
import train.book.R;
import train.book.activities.edit.WorkoutEditBaseActivity;
import train.book.data.EditExerciseData;
import train.book.data.ExerciseData;
import train.book.data.WorkoutData;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class ExerciseHistoryActivity extends CustomWindow {
	private ExerciseHistoryAdapter exerciseList;
	private TextView title;
	private EditText comment;

	private static EditExerciseData editData;

	private WorkoutData currentData;

	private static final int ID_EDIT = 16;
	private static final int ID_DELETE = 17;

	private static final int EDIT_EXERCISE = 6;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.exercise_history);

		currentData = HistoryActivity.getEditData();

		title = (TextView) findViewById(R.id.textViewExerciseHistoryTitle);
		comment = (EditText) findViewById(R.id.editTextExerciseHistoryComment);

		exerciseList = new ExerciseHistoryAdapter(this);
		ListView lv = (ListView) findViewById(R.id.listViewExerciseHistory);
		lv.setAdapter(exerciseList);

		// TODO MED: Delete option in menu?

		// TODO LOW: Edit date.

		// Fill the list view
		title.setText(currentData.getWorkoutName()
				+ " - "
				+ Const.date_medium_format.format(new Date(currentData
						.getTime())));
		comment.setText(currentData.getComment());
		exerciseList.setData(currentData);

		// Add long click thing for the list view
		registerForContextMenu(lv);

		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				editExerciseData((ExerciseData) parent
						.getItemAtPosition(position));
			}
		});
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		if (v.getId() == R.id.listViewExerciseHistory) {
			menu.setHeaderTitle(getString(R.string.title_manage_result));
			menu.add(Menu.NONE, ID_EDIT, Menu.NONE, R.string.workout_edit);
			menu.add(Menu.NONE, ID_DELETE, Menu.NONE, R.string.workout_delete);
		} else {
			super.onCreateContextMenu(menu, v, menuInfo);
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case ID_EDIT: {
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
					.getMenuInfo();
			ExerciseData d = exerciseList.getItem(info.position);
			editExerciseData(d);
			return true;
		}
		case ID_DELETE: {
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
					.getMenuInfo();
			ExerciseData d = exerciseList.getItem(info.position);
			currentData.delete(d);
			exerciseList.remove(d);
			return true;
		}
		default:
			return super.onContextItemSelected(item);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == EDIT_EXERCISE) {
			if (resultCode == RESULT_OK) {
				editData.commit();
				exerciseList.notifyDataSetChanged();
			} else {
				editData.revert();
			}
		}
	}

	public void onClickSave(View v) {
		currentData.setComment(comment.getText().toString());
		setResult(RESULT_OK);
		finish();
	}

	public void onClickCancel(View v) {
		setResult(RESULT_CANCELED);
		finish();
	}

	private void editExerciseData(ExerciseData d) {
		editData = new EditExerciseData(d);
		editData.startEditing();
		Intent intent = new Intent(ExerciseHistoryActivity.this,
				WorkoutEditBaseActivity.getActivity(d.getType()));
		startActivityForResult(intent, EDIT_EXERCISE);
	}

	public static EditExerciseData getEditData() {
		return editData;
	}
}
