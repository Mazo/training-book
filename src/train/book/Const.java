package train.book;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;

public class Const {
	public static final double DEFAULT_WEIGHT = 70.0;
	public static final String DEFAULT_EXERCISE_WEIGHT = "30.0";
	public static final String DEFAULT_EXERCISE_REPS = "8";
	public static final String UNKNOWN_NAME = "Unknown";
	public static final String WEIGHT_UNIT = "kg";
	public static final String TIME_UNIT = "min";
	public static final String DISTANCE_UNIT = "km";
	public static final String SPEED_UNIT = "km/h";

	public static final DecimalFormat one_decimal_format = new DecimalFormat(
			"#0.0");

	public static final DateFormat date_medium_format = DateFormat
			.getDateInstance(DateFormat.MEDIUM);
	public static final Format chart_axis_format = date_medium_format;
	public static final String DEFAULT_TIMEZONE = "Europe/Stockholm";
	public static final double WEIGHT_INCREASE = 0.1;

}
