package train.book;

import android.content.Context;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public class NumberDecimalPicker extends LinearLayout {
	private EditText editText;
	protected double value = 0;
	private static final int DELAY = 50;
	private boolean autoDec = false;
	private boolean autoInc = false;
	private Handler repeatHandler = new Handler();
	private double increaseValue = 0.5;
	private static double DECIMAL_FIX = 1000; // 3 decimals

	public NumberDecimalPicker(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflater.inflate(R.layout.custom_numpicker, null);
		addView(v);

		editText = (EditText) findViewById(R.id.custNumPickerEditText);
		Button dec = (Button) findViewById(R.id.custNumPickerButtonDec);
		Button inc = (Button) findViewById(R.id.custNumPickerButtonInc);

		editText.addTextChangedListener(new TextWatcher() {

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				updateValue();
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void afterTextChanged(Editable s) {
			}
		});

		dec.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dec();
			}
		});
		dec.setOnLongClickListener(new OnLongClickListener() {
			public boolean onLongClick(View v) {
				autoDec = true;
				repeatHandler.post(new Updater());
				return true;
			}
		});
		dec.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP && autoDec) {
					autoDec = false;
				}
				return false;
			}
		});

		inc.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				inc();
			}
		});
		inc.setOnLongClickListener(new OnLongClickListener() {
			public boolean onLongClick(View v) {
				autoInc = true;
				repeatHandler.post(new Updater());
				return true;
			}
		});
		inc.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP && autoInc) {
					autoInc = false;
				}
				return false;
			}
		});
	}

	public void addTextChangedListener(TextWatcher watcher) {
		editText.addTextChangedListener(watcher);
	}

	public void setIncreaseValue(double increaseValue) {
		this.increaseValue = increaseValue;
	}

	protected double getIncreaseValue() {
		return increaseValue;
	}

	private void dec() {
		value -= getIncreaseValue();
		value = Math.round(value * DECIMAL_FIX) / DECIMAL_FIX;
		updateText();
	}

	private void inc() {
		value += getIncreaseValue();
		value = Math.round(value * DECIMAL_FIX) / DECIMAL_FIX;
		updateText();
	}

	private void updateText() {
		if (value < 0) {
			value = 0;
		}
		// Save the selection
		int start = editText.getSelectionStart();
		int end = editText.getSelectionEnd();
		String newText = getText();
		editText.setText(newText);
		int len = newText.length();
		if (start > len) {
			start = len;
		}
		if (end > len) {
			end = len;
		}
		editText.setSelection(start, end);
	}

	protected String getText() {
		return Double.toString(value);
	}

	public void setValue(double value) {
		this.value = value;
		updateText();
	}

	private void updateValue() {
		double oldValue = value;
		try {
			value = Double.parseDouble(editText.getText().toString());
			value = Math.round(value * DECIMAL_FIX) / DECIMAL_FIX;
		} catch (NumberFormatException e) {
			value = oldValue;
		}
	}

	public double getValue() {
		updateValue();
		return value;
	}

	private class Updater implements Runnable {
		public void run() {
			if (autoDec) {
				dec();
				repeatHandler.postDelayed(Updater.this, DELAY);
			} else if (autoInc) {
				inc();
				repeatHandler.postDelayed(Updater.this, DELAY);
			}
		}

	}
}
