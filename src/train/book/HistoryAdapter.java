package train.book;

import java.util.Date;
import java.util.List;

import train.book.data.HistoryData;
import train.book.data.WeightData;
import train.book.data.WorkoutData;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class HistoryAdapter extends ArrayAdapter<HistoryData> {

	public HistoryAdapter(Context context, List<HistoryData> list) {
		super(context, R.layout.history_table_row, list);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = View.inflate(getContext(),
					R.layout.history_table_row, null);
		}
		HistoryData d = getItem(position);
		if (d != null) {
			TextView date = (TextView) convertView
					.findViewById(R.id.textViewHistoryTableDate);
			TextView type = (TextView) convertView
					.findViewById(R.id.textViewHistoryTableType);
			date.setText(Const.date_medium_format.format(new Date(d.getTime())));
			if (d instanceof WorkoutData) {
				type.setText(((WorkoutData) d).getWorkoutName());
			} else if (d instanceof WeightData) {
				type.setText(getContext().getString(
						R.string.history_weight_type));
			}
		}
		return convertView;
	}
}
